terraform {
  source = "${get_path_to_repo_root()}//modules/service/ddns_updater"
}

include "root" {
  path = find_in_parent_folders()
}
