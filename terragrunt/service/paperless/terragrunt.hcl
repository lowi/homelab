terraform {
  source = "${get_path_to_repo_root()}//modules/service/paperless"
}

include "root" {
  path = find_in_parent_folders()
}
include "paperless" {
  path = find_in_parent_folders("paperless.hcl")
}
