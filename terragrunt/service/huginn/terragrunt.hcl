terraform {
  source = "${get_path_to_repo_root()}//modules/service/huginn"
}

include "root" {
  path = find_in_parent_folders()
}

include "nomad_provider" {
  path = find_in_parent_folders("nomad_provider.hcl")
}

inputs = {
}

