terraform {
  source = "${get_path_to_repo_root()}//modules/service/media/readarr"
}

include "root" {
  path = find_in_parent_folders()
}

include "piracy" {
  path = find_in_parent_folders("piracy.hcl")
}
