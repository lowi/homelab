terraform {
  source = "${get_path_to_repo_root()}//modules/service/debug/debug_connect_02"
}
include "root" {
  path = find_in_parent_folders()
}

# include "media" {
#   path = find_in_parent_folders("media.hcl")
# }

include "piracy" {
  path = find_in_parent_folders("piracy.hcl")
}
