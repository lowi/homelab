terraform {
  source = "${get_path_to_repo_root()}//modules/service/security/vaultwarden"
}

include "root" {
  path = find_in_parent_folders()
}
