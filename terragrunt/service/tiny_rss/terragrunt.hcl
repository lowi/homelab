terraform {
  source = "${get_path_to_repo_root()}//modules/service/tiny_rss"
}

include "root" {
  path = find_in_parent_folders()
}
