terraform {
  source = "${get_path_to_repo_root()}//modules/service/syncthing"
}

include "root" {
  path = find_in_parent_folders()
}

inputs = {
  config_dir = "/opt/jumpstr/syncthing"
  volume_id  = "syncthing"
}

