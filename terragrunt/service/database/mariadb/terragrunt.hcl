terraform {
  source = "${get_path_to_repo_root()}//modules/service/database/maria-db"
}

include "root" {
  path = find_in_parent_folders()
}

include "maria_db" {
  path = find_in_parent_folders("mariadb.hcl")
}
