terraform {
  source = "${get_path_to_repo_root()}//modules/service/database/postgres"
}

include "root" {
  path = find_in_parent_folders()
}

include "postgres" {
  path = find_in_parent_folders("postgres.hcl")
}
