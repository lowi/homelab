
locals {
  media_base_folder      = "/media/me/4TB_02/media"
  video_base_folder      = "${local.media_base_folder}/video"
  series_base_folder     = "${local.video_base_folder}/serien"
  movies_base_folder     = "${local.video_base_folder}/film"
  books_base_folder      = "${local.media_base_folder}/books"
  audiobooks_base_folder = "${local.media_base_folder}/audiobooks"
  music_base_folder      = "${local.media_base_folder}/music"
  torrent_base_folder    = "${local.media_base_folder}/torrents"
}

inputs = local
