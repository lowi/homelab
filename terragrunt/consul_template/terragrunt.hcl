terraform {
  source = "${get_path_to_repo_root()}//modules/consul_template"
}

// Include the root, which sets up the remote_state backend
// and imports common variables
include "root" {
  path = find_in_parent_folders()
}

include "pki" {
  path = find_in_parent_folders("pki.hcl")
}

include "secrets" {
  path = find_in_parent_folders("secrets.hcl")
}

include "consul_template" {
  path = find_in_parent_folders("consul_template.hcl")
}

inputs = {
  vault_address = "https://10.0.0.4:8200"
}
