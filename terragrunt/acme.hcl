
locals {

  variables = read_terragrunt_config(find_in_parent_folders()).locals

  acme_provider  = "namecheap"
  namecheap_user = get_env("NAMECHEAP_USER")
  namecheap_key  = get_env("NAMECHEAP_KEY")
  // ssl_cert_email = "letsencrypt@${local.variables.external_domain}"
  // acme_json      = get_env("ACME_JSON")

}

// generate "acme_variables" {
//   path      = "acme_variables.tf"
//   if_exists = "overwrite_terragrunt"
//   contents  = templatefile(
//   "../templates/variables/acme.tf", {})
// }

inputs = {
  acme_provider  = local.acme_provider
  namecheap_user = local.namecheap_user
  namecheap_key  = local.namecheap_key
}
