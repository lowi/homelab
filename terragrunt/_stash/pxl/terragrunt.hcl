terraform {
  source = "${get_path_to_repo_root()}//modules/pxl"
}

include "root" {
  path = find_in_parent_folders()
}

include "pixelstreaming" {
  path = find_in_parent_folders("pixelstreaming.hcl")
}

include "nomad_provider" {
  path = find_in_parent_folders("nomad_provider.hcl")
}

inputs = {
  twilio_id = get_env("TWILIO_ACCOUNT_SID")
  twilio_token = get_env("TWILIO_AUTH_TOKEN")
}
