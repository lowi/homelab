terraform {
  source = "${get_path_to_repo_root()}//modules/nomad-jobs/postgres"
}

include "root" {
  path = find_in_parent_folders()
}

include "domain" {
  path = find_in_parent_folders("domain.hcl")
}

include "consul" {
  path = find_in_parent_folders("consul.hcl")
}

include "nomad" {
  path = find_in_parent_folders("nomad.hcl")
}

include "csi" {
  path = find_in_parent_folders("csi.hcl")
}


inputs = {
  postgres_db       = get_env("JUMPSTR_POSTGRES_DB"),
  postgres_password = get_env("JUMPSTR_POSTGRES_PASSWORD"),
  postgres_user     = get_env("JUMPSTR_POSTGRES_USER"),
}
