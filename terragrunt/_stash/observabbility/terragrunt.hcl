terraform {
  source = "${get_path_to_repo_root()}//modules/nomad-observability"
}

include "root" {
  path = find_in_parent_folders()
}

# locals {
#   region_vars  = read_terragrunt_config(find_in_parent_folders("region.hcl"))
#   gluster_vars = read_terragrunt_config(find_in_parent_folders("gluster-server.hcl"))
# }

# inputs = {
#   datacenter            = local.region_vars.locals.datacenter
#   nomad_region          = local.region_vars.locals.region
#   nomad_url             = local.region_vars.locals.nomad_url
#   consul_url            = local.region_vars.locals.consul_url
#   public_base_url       = local.region_vars.locals.public_base_url
#   service_domain = local.region_vars.locals.service_domain

#   volume_name         = local.gluster_vars.locals.volume_name
#   gluster_hosts       = local.gluster_vars.locals.gluster_hosts
#   gluster_volume_name = local.gluster_vars.locals.gluster_volume_name
# }
