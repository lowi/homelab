terraform {
  source = "${get_path_to_repo_root()}//modules/nomad-jobs/home-assistant"
}

include "root" {
  path = find_in_parent_folders()
}

include "domain" {
  path = find_in_parent_folders("domain.hcl")
}

include "consul" {
  path = find_in_parent_folders("consul.hcl")
}

include "nomad" {
  path = find_in_parent_folders("nomad.hcl")
}
