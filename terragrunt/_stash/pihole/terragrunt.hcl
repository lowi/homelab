terraform {
  source = "${get_path_to_repo_root()}//modules/service/pihole"
}

include "root" {
  path = find_in_parent_folders()
}

include "nomad_provider" {
  path = find_in_parent_folders("nomad_provider.hcl")
}

inputs = {
  pihole_base_folder = "/opt/jumpstr/pihole"
  pihole_web_password = get_env("PIHOLE_WEB_PASSWORD")
}

