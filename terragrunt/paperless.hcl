inputs = {
  // paperless_config_path = "paperless"
  paperless_secret_path = "secret/data/paperless"

}

generate "paperless_variables" {
  path      = "paperless_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
// variable "paperless_config_path" {
//   default     = "paperless/config"
//   description = "Consul-KV path to the static configuration"
// }
variable "paperless_secret_path" {
  type        = string
  description = "Path to the paperless secret in Vault"
}

EOF
}