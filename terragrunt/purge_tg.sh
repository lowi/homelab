#/bin/sh
find . -type d -name '.terragrunt-cache' -exec rm -r {} +
find . -name '.terraform.lock.hcl' -exec rm -r {} +
