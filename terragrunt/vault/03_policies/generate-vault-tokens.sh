#!/bin/sh

# sudo cp /etc/ssl/private/vault.key $SECRETS_BASE_PATH/keys/vault.key
# sudo chown me:me $SECRETS_BASE_PATH/keys/vault.key


policies=$(terragrunt show -json | jq '.values.root_module.resources | map(select(.type == "vault_policy"))' | jq '.[].values.name' -r)

echo $policies

for policy_name in $policies
do
  token=$(vault token create -policy=$policy_name -format=json)
	echo "token: $token"

done


