terraform {
  source = "${get_path_to_repo_root()}//modules/vault/pki/01_root_ca/02_import_certificate"
}

// Include the root, which sets up the remote_state backend
// and imports common variables
include "root" {
  path = find_in_parent_folders()
}

include "pki" {
  path = find_in_parent_folders("pki.hcl")
}
