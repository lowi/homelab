#!/bin/sh

# https://learn.hashicorp.com/tutorials/vault/pki-engine-external-ca?in=vault/secrets-management#step-3-generate-ica1-in-vault

# because we're using terragrunt, we have to be inside the tg direcotry to be able to read the terraform state
# ? I think. havent found a better way
tg_folder=/home/me/code/servers/jumpstr/terraform/terragrunt/vault/pki/01_root_ca/01_prepare_pki
cd $tg_folder

pwd

# prepare a temporary folder for the csr
mkdir vault
# copy the root ca to a tmp folder
cp -r $SECRETS_BASE_PATH/pki/offline_root_ca out

# read the terraform state and parse the certificate request
# store the request in a file 'vault_ica.csr'
terragrunt show -json | jq '.values["root_module"]["resources"][].values.csr' -r | grep -v null > vault_ica.csr

# Sign with the offline root
# --cert: path to certificate output PEM file
certstrap sign \
  --expires "3 year" \
  --csr vault_ica.csr \
  --cert vault/vault_ica.crt \
  --intermediate \
  --passphrase "$ROOT_CA_PASSPHRASE" \
  --CA "jumpstr Root CA" \
  --path-length 1 \
  "Intermediate Vault CA"

# # Generate the cerificate chain
cat vault/vault_ica.crt > vault/vault_ica.chain.crt
cat out/jumpstr_Root_CA.crt >> vault/vault_ica.chain.crt

# # Copy in place
mv vault $SECRETS_BASE_PATH/pki/vault_root_ica

# clean up
rm -f vault_ica.csr
rm -rf out
