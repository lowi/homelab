terraform {
  source = "${get_path_to_repo_root()}//modules/vault/secrets"
}

// Include the root, which sets up the remote_state backend
// and imports common variables
include "root" {
  path = find_in_parent_folders()
}

include "secrets" {
  path = find_in_parent_folders("secrets.hcl")
}

include "pki" {
  path = find_in_parent_folders("pki.hcl")
}
