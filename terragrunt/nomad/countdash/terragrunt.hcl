terraform {
  source = "${get_path_to_repo_root()}//modules/countdash"
}

include "root" {
  path = find_in_parent_folders()
}

include "media" {
  path = find_in_parent_folders("media.hcl")
}

inputs = {
  config_dir = "/opt/jumpstr/countdash"
}

