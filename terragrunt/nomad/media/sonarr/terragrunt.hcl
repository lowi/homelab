terraform {
  source = "${get_path_to_repo_root()}//modules/sonarr"
}

include "root" {
  path = find_in_parent_folders()
}

include "media" {
  path = find_in_parent_folders("media.hcl")
}

include "nomad_provider" {
  path = find_in_parent_folders("nomad_provider.hcl")
}

inputs = {
  config_dir = "/opt/jumpstr/sonarr"
}

