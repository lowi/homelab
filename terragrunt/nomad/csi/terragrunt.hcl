terraform {
  # source = "${get_path_to_repo_root()}//modules/nomad/kadalu-csi"
  source = "../..//modules/nomad/kadalu-csi"
}

include "root" {
  path = find_in_parent_folders()
}

# Inclue domain specific data
# include "domain" {
#   path = find_in_parent_folders("domain.hcl")
# }

include "csi" {
  path = find_in_parent_folders("csi.hcl")
}

include "nomad_job" {
  path = find_in_parent_folders("nomad_job.hcl")
}
