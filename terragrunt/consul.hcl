
locals {

  template_path = "../templates"
  domain_vars   = read_terragrunt_config("${get_parent_terragrunt_dir()}/domain.hcl").locals

  provider = templatefile(
    "${local.template_path}/providers/consul.tf",
    {
      address    = local.consul_cluster_url,
      datacenter = local.domain_vars.datacenter,
  })

  variables = templatefile(
    "${local.template_path}/variables/consul.tf",
    {}
  )

  consul_domain      = "consul"
  service_domain     = "service.${local.consul_domain}"
  consul_tag         = "consul"
  consul_cluster_url = "${local.consul_tag}.${local.service_domain}:8500"
  consul_hostnames   = ["mini"]

  consul_image = "ignore"

  consul_gossip_encryption_key        = get_env("CONSUL_GOSSIP_ENCRYPTION_KEY")
  consul_acl_agent_token              = get_env("CONSUL_AGENT_TOKEN")
  consul_acl_agent_recovery_token     = get_env("CONSUL_AGENT_RECOVERY_TOKEN")
  consul_acl_default_token            = get_env("CONSUL_DEFAULT_TOKEN")
  consul_acl_initial_management_token = get_env("CONSUL_INITIAL_MANAGEMENT_TOKEN")
}

generate "provider" {
  path      = "consul_provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = local.provider
}

generate "consul_variables" {
  path      = "consul_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = local.variables
}

inputs = local
