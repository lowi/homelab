inputs = {
  postgres_volume_id   = "psql"
  postgres_secret_path = "secret/data/postgresql"

}

generate "postgres_variables" {
  path      = "postgres_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
variable "postgres_volume_id" {
  type        = string
  description = "The ID of the data volume"
}
variable "postgres_secret_path" {
  type        = string
  description = "Path to the postgres secret in Vault"
}

EOF
}