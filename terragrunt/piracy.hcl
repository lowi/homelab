inputs = {
  media_node_class        = "lenovo"
  media_library_volume    = "media_library"
  torrent_watch_volume    = "torrent_watch"
  torrent_download_volume = "torrent_downloads"

  transmission_config_volume = "transmission_config"
  jackett_config_volume      = "jackett_config"
  sonarr_config_volume       = "sonarr_config"
  radarr_config_volume       = "radarr_config"
  lidarr_config_volume       = "lidarr_config"
  readarr_config_volume      = "readarr_config"
  jellyfin_config_volume     = "jellyfin_config"
  calibre_config_volume      = "calibre_config"
  media_puid                 = 911
  media_pgid                 = 911
}

generate "piracy_variables" {
  path      = "piracy_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
variable "media_node_class" {
  type        = string
  description = "The node class of the Nomad node"
}

variable "media_library_volume" {
  type        = string
  description = "The ID of the media library data volume"
}

variable "torrent_watch_volume" {
  type        = string
  description = "The ID of the data volume for watching torrents"
}
variable "torrent_download_volume" {
  type        = string
  description = "The ID of the data volume for downloaded torrents"
}

// * Config volumes
variable "transmission_config_volume" {
  type        = string
  description = "The ID of volume containing the configuration for Transmission"
}
variable "jackett_config_volume" {
  type        = string
  description = "The ID of volume containing the configuration for Jackett"
}
variable "sonarr_config_volume" {
  type        = string
  description = "The ID of volume containing the configuration for Sonarr"
}
variable "radarr_config_volume" {
  type        = string
  description = "The ID of volume containing the configuration for Radarr"
}
variable "lidarr_config_volume" {
  type        = string
  description = "The ID of volume containing the configuration for lidarr"
}
variable "readarr_config_volume" {
  type        = string
  description = "The ID of volume containing the configuration for readarr"
}
variable "jellyfin_config_volume" {
  type        = string
  description = "The ID of volume containing the configuration for jellyfin"
}
variable "calibre_config_volume" {
  type        = string
  description = "The ID of volume containing the configuration for calibre"
}

// * User and group definitions
// ! we need to run each media container under the same user
// ! that is because we want to share the media library volume
// ! and different containers reading/writing to the same volume is APITA
variable "media_puid" {
  description = "Id of the task-user"
}
variable "media_pgid" {
  description = "Id of the task-user group"
}
EOF
}
