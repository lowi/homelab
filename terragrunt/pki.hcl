
locals {
  variables             = read_terragrunt_config(find_in_parent_folders()).locals
  root_pki_path         = get_env("VAULT_ROOT_CA_PATH")
  intermediate_pki_path = get_env("VAULT_INTERMEDIATE_CA_PATH")
}

inputs = {
  root_pki_path         = local.root_pki_path
  intermediate_pki_path = local.intermediate_pki_path

  consul_role  = "consul"
  connect_role = "connect"
  nomad_role   = "nomad-cluster"

  // intermediate_ica_vault_role   = "vault"

  # Path to the Vault intermediate CA certificate (signed by offline root)
  vault_ica_certificate = get_env("VAULT_CA_CERT_FILE")

  vault_address           = get_env("VAULT_ADDR")
  vault_token_for_connect = get_env("VAULT_TOKEN_FOR_CONNECT")

  // nomad_policy_name = "nomad-tls"
}


generate "duration_locals" {
  path      = "duration_locals.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<LOCALS
locals {
  ten_years_in_seconds   = 315360000
  five_years_in_seconds  = 157680000
  three_years_in_seconds = 94608000
  one_year_in_seconds    = 31536000
  one_month_in_seconds   = 2592000
  one_day_in_seconds     = 86400
  one_hour_in_seconds    = 3600
  three_years_in_hours   = 26298

}
LOCALS
}


generate "pki_variables" {
  path      = "pki_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<VARIABLES

# Folders
# ————————————————————————————————————————————————————————————————
variable "certificates_base_folder" {
  type        = string
  description = "The Filesystem directory where TLS certificates are stored"
  default     = "/usr/local/share/ca-certificates"
}

variable "private_keys_base_folder" {
  type        = string
  description = "The Filesystem directory whereprivate keys are stored"
  default     = "/etc/ssl/private"
}

# Vault cluster
# ————————————————————————————————————————————————————————————————
variable "vault_address" {
  type        = string
  description = "URL of the Vault cluster (including protocoll)"
  default     = ""
}


# Root CA
# ————————————————————————————————————————————————————————————————

variable "vault_ica_certificate" {
  description = "Path to the Vault ICA certificate chain"
  type        = string
}

variable "root_pki_path" {
  description   = "Vault path to the root pki"
  type          = string
  default       = "root-ca"
}


# Intermediate CA
# ————————————————————————————————————————————————————————————————

variable "intermediate_pki_path" {
  description = "Path to the Vault ICA certificate chain"
  type        = string
}

variable "consul_role" {
  description = "Role for issuing certificates for Consul agents"
  type        = string
  default     = "consul"
}

variable "connect_role" {
  description = "Role for issuing certificates for Consul Connect"
  type        = string
  default     = "connect"
}

variable "nomad_role" {
  description = "Rose for issuing certificates for Nomad agents"
  type        = string
  default     = "nomad"
}


# Connect CA
# ————————————————————————————————————————————————————————————————

variable "vault_token_for_connect" {
  description = "Vault access token for consul connect"
  type        = string
  default     = null
}

variable "nomad_policy_name" {
  description = "Vault TLS policy for Nomad"
  type        = string
  default     = "nomad-tls"
}

variable "ca_subject" {
  description = "The `subject` block for the root CA certificate."
  type = object({
    organization        = string,
    organizational_unit = string,
    street_address      = list(string),
    locality            = string,
    province            = string,
    country             = string,
    postal_code         = string,
  })

  default = {
    organization        = "jumpstr"
    organizational_unit = "Department of Certificate Authority"
    street_address      = ["Springergasse 2/29"]
    locality            = "Vienna"
    province            = "Vienna"
    country             = "AT"
    postal_code         = "1020"
  }
}
VARIABLES
}
