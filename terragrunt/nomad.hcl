
locals {
  template_path = "../../templates"

  domain_vars = read_terragrunt_config("${get_parent_terragrunt_dir()}/domain.hcl").locals
  consul_vars = read_terragrunt_config("${get_parent_terragrunt_dir()}/consul.hcl").locals

  provider = templatefile(
    "${local.template_path}/providers/nomad.tf",
    {
      address = local.nomad_cluster_url,
      region  = local.domain_vars.location,
  })

  variables = templatefile(
  "${local.template_path}/variables/nomad.tf", {})

  nomad_tag         = "nomad"
  nomad_hostnames   = ["mini"]
  nomad_cluster_url = "http://${local.nomad_tag}.${local.consul_vars.service_domain}:4646"
  nomad_prefix      = "nomad"
  // we're not starting a VM but attaching to a running node,
  // so we can ignore the image
  nomad_image = "ignore"
}

generate "nomad_provider" {
  path      = "nomad_provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = local.provider
}

generate "nomad_variables" {
  path      = "nomad_variables.tf"
  contents  = local.variables
  if_exists = "overwrite_terragrunt"
}

inputs = local
