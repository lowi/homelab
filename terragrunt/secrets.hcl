
locals {
  vault_secrets_path = "secret"
}

inputs = local

generate "secrets_variables" {
  path      = "secrets_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF

variable "vault_secrets_path" {
  type        = string
  default     = "secrets"
  description = "Vault mount point of the key-value store where our secrets are kept"
}

EOF
}
