inputs = {
  mariadb_volume_id   = "mysql"
  mariadb_secret_path = "secret/data/mariadb"
}

generate "mariadb_variables" {
  path      = "mariadb_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
variable "mariadb_volume_id" {
  type        = string
  description = "The ID of the data volume"
}
variable "mariadb_secret_path" {
  type        = string
  description = "Path to the MariaDB secrets in Vault"
}
EOF
}