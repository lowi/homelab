locals {
  template_path = "../templates"

  # domain_vars = read_terragrunt_config("${get_parent_terragrunt_dir()}/domain.hcl").locals


  variables = templatefile(
  "${local.template_path}/variables/nomad_job.tf", {})

  consul_provider = templatefile(
    "${local.template_path}/providers/consul.tf",
    {
      // address    = local.consul_cluster_url,
      address    = "http://consul.${local.domain_vars.service_domain}:8500",
      datacenter = local.domain_vars.datacenter,
  })

  nomad_provider = templatefile(
    "${local.template_path}/providers/nomad.tf",
    {
      address = "http://nomad.${local.domain_vars.service_domain}:4646",
      region  = local.domain_vars.datacenter,
  })

  nomad_job_tags = []
}

inputs = local

generate "nomad_provider" {
  path      = "nomad_provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = local.nomad_provider
}

generate "consul_provider" {
  path      = "consul_provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = local.consul_provider
}

generate "nomad_job_variables" {
  path      = "nomad_job_variables.tf"
  contents  = local.variables
  if_exists = "overwrite_terragrunt"
}

