
inputs = {
  vault_token_for_consul_template = get_env("VAULT_TOKEN_FOR_CONSUL_TEMPLATE")
}



generate "consul_template_variables" {
  path      = "consul_template_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF

variable "vault_token_for_consul_template" {
  type        = string
  description = "Vault token for Consul Template"
}

EOF
}
