

generate "pixelstreaming_variables" {
  path      = "pixelstreaming_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<VARIABLES

variable "pixelstreaming_unreal_port" {
  type        = number
  description = "Websocket port for Unreal engines"
  default     = 4712
}

variable "pixelstreaming_player_port" {
  type        = number
  description = "Websocket port for players"
  default     = 4713
}

variable "twilio_id" {
  type        = string
}

variable "twilio_token" {
  type        = string
}
VARIABLES
}

