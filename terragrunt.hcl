locals {
  namespace          = "jumpstr"
  region             = local.namespace
  datacenter         = local.namespace
  external_domain    = "lowi.xyz"
  internal_domain    = "jump"
  consul_domain      = "consul"
  service_domain     = "service.consul"
  nomad_domain       = "nomad"
  config_base_folder = "/opt/jumpstr"
}

inputs = local

generate "domain_variables" {
  path      = "domain_variables.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<VARIABLES

variable "namespace" {
  type        = string
  description = "Our namespace"
}

variable "external_domain" {
  type        = string
  description = "Public domain name"
}

variable "internal_domain" {
  type        = string
  description = "Internal LAN domain name"
}

variable "service_domain" {
  type        = string
  description = "Internal domain for consul services"
}

variable "consul_domain" {
  type        = string
  description = "Internal domain for Consul"
  default     = "dc1.consul"
}

variable "nomad_domain" {
  type        = string
  description = "Internal domain for Nomad"
  default     = "global.nomad"
}

variable "datacenter" {
  type        = string
  description = "Name of our datacenter"
  default     = "dc1"
}

variable "region" {
  type        = string
  description = "Region of our datacenter"
  default     = "global"
}

variable "config_base_folder" {
  type        = string
  description = "Basefolder for configuration"
  default     = "/opt/jumpstr"
}
VARIABLES
}


generate "nomad_provider" {
  path      = "nomad_provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<PROVIDER
provider "nomad" {
  address      = "${get_env("NOMAD_HTTP_ADDR")}"
  region       = "${local.region}"
  ca_file      = "${get_env("NOMAD_CACERT")}"
  cert_file    = "${get_env("NOMAD_CLIENT_CERT")}"
  key_file     = "${get_env("NOMAD_CLIENT_KEY")}"

}
PROVIDER
}


generate "vault_provider" {
  path      = "vault_provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<PROVIDER
provider "vault" {
  address      = "${get_env("VAULT_ADDR")}"
  token       = "${get_env("VAULT_ROOT_TOKEN")}"
}
PROVIDER
}
