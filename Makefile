# adapted from:
# https://medium.com/nubego/how-to-run-terraform-like-a-pro-in-a-devops-world-c21458ba402c

# We  prefer that all the lines in the recipe be passed to a single invocation of the shell
# [.ONESHELL](https://www.gnu.org/software/make/manual/html_node/One-Shell.html) does that.
# see: https://stackoverflow.com/questions/1789594/how-do-i-write-the-cd-command-in-a-makefile/30590240#30590240l.
.ONESHELL: # Applies to every targets in the file!

################################################
# VARIABLES
################################################
MODULE = all

################################################
# MODULES
################################################
# .PHONY: all
# all:
# 	$(eval MODULE = all)

# [Container Storage Interface](https://github.com/container-storage-interface/spec/blob/master/spec.md)
.PHONY: csi
csi:
	$(eval MODULE = csi)

# Ingres proxy
.PHONY: traefik
traefik:
	$(eval MODULE = traefik)

.PHONY: postgres
postgres:
	$(eval MODULE = postgres)

.PHONY: matrix
matrix:
	$(eval MODULE = matrix)

.PHONY: matrix/synapse
matrix/synapse:
	$(eval MODULE = matrix/synapse)

.PHONY: matrix/element
matrix/element:
	$(eval MODULE = matrix/element)

################################################
# TARGETS
################################################

.PHONY: plan
plan:
	export MODLUE=${MODULE}
	cd ./terragrunt/$(MODULE)
	terragrunt plan ${ARGS}

.PHONY: apply
apply:
	export MODLUE=${MODULE}
	cd ./terragrunt/$(MODULE)
	terragrunt apply ${ARGS} --auto-approve

.PHONY: destroy
destroy:
	export MODLUE=${MODULE}
	cd ./terragrunt/$(MODULE)
	terragrunt destroy ${ARGS}  --auto-approve

.PHONY: plan-all
plan-all:
	export MODLUE=${MODULE}
	cd ./terragrunt/$(MODULE)
	terragrunt run-all plan ${ARGS}

.PHONY: apply-all
apply-all:
	export MODLUE=${MODULE}
	cd ./terragrunt/$(MODULE)
	terragrunt run-all apply ${ARGS}

.PHONY: destroy-all
destroy-all:
	export MODLUE=${MODULE}
	cd ./terragrunt/$(MODULE)
	terragrunt run-all destroy ${ARGS}

.PHONY: fmt
fmt:
	terragrunt hclfmt

################################################
# ACTIONS
################################################

## local dev ##
# jumpstr-dev-network-plan: local dev network plan
# jumpstr-dev-consul-plan: local dev consul plan
# jumpstr-dev-apply: local dev apply


# Google compute platform

## Development

#### Workloads
# (Orchestrated by nomad)
workloads-plan: workloads plan-all
workloads-apply: workloads apply-all
workloads-destroy: workloads destroy-all

##### Traefik ingres proxy
traefik-plan: traefik plan
traefik-apply: traefik apply
traefik-destroy: traefik destroy

##### Container storage interface (CSI)
csi-plan: csi plan
csi-apply: csi apply
csi-destroy: csi destroy

##### Postgres database
postgres-plan: postgres plan
postgres-apply: postgres apply
postgres-destroy: postgres destroy

### MATRIX

#### Synapse matrix server
synapse-plan: matrix/synapse plan
synapse-apply: matrix/synapse apply
synapse-destroy: matrix/synapse destroy

#### Element matrix client
element-plan: matrix/element plan
element-apply: matrix/element apply
element-destroy: matrix/element destroy
