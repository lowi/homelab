
resource "nomad_job" "countdash" {
  jobspec = file("${path.module}/countdash.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter       = var.datacenter,
      region           = var.region,
    }
  }
}
