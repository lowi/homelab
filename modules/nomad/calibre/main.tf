terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "calibre" {
  jobspec = file("${path.module}/calibre.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.namespace
      config_dir      = var.config_dir
      torrent_dir     = var.torrent_base_folder
      /* external_domain = var.external_domain */
      internal_domain = var.internal_domain
    }
  }
}
