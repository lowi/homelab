variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "config_dir" {
  description = "Directory for data used by the app"
}

variable "torrent_dir" {
  description = "Torrent directory"
}

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base domain"
}

// variable "external_domain" {
//   default     = "example.com"
//   description = "Public base domain"
// }

job "calibre" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = "${node.class}"
    value     = "zwei"
  }

  group "calibre" {
    network {
      mode = "bridge"
      port "desktop" {
        static = 8080
        to = 8080
      }
      port "webserver" {
        static = 8081
        to = 8081
      }

    }

    service {
      name = "calibre"
      port = "desktop"
      task = "calibre"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.calibre.entrypoints=web",
        "traefik.http.routers.calibre.rule=Host(`calibre.${var.internal_domain}`)",
        "traefik.http.routers.calibre.service=calibre",
        "traefik.http.services.calibre.loadbalancer.server.port=${NOMAD_HOST_PORT_desktop}",
      ]

      connect {
        sidecar_service {}
      }
    }

    task "calibre" {
      driver = "docker"
      config {
        image = "linuxserver/calibre"
        ports = [ "webserver", "desktop" ]
        volumes = [
          "${ var.config_dir }:/config",
          // "${ var.book_dir }:/books",
        ]
      }
    }
  }
}
