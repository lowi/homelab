
variable "config_dir" {
  description = "Directory for data used by the app"
}

variable "movies_base_folder" {
  description = "TV shows directory"
}

variable "torrent_base_folder" {
  description = "Torrent directory"
}
