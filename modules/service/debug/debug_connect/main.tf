terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "debug_connect" {
  jobspec = file("${path.module}/debug_connect.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.namespace
    }
  }
}
