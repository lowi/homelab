variable "region" {
  default = "jumpstr"
}

variable "datacenter" {
  description = "Data Center that the job is running in"
  default     = "jumpstr"
}

job "countdash_api" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  group "api" {
    network {
      mode = "bridge"
    }

    service {
      name = "count-api"
      port = "9001"

      connect {
        sidecar_service {}
      }
    }

    task "web" {
      driver = "docker"
      config {
        image = "hashicorpdev/counter-api:v3"
      }
    }
  }
}
