variable "region" {
  default = "jumpstr"
}

variable "datacenter" {
  description = "Data Center that the job is running in"
  default     = "jumpstr"
}

job "countdash_dashboard" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  group "dashboard" {
    network {
      mode = "bridge"
      port "http" {
        static = 9002
        to     = 9002
      }
    }

    service {
      name = "count-dashboard"
      port = "http"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "count-api"
              local_bind_port  = 8080
            }
          }
        }
      }
    }

    task "dashboard" {
      driver = "docker"
      env {
        COUNTING_SERVICE_URL = "http://${NOMAD_UPSTREAM_ADDR_count_api}"
      }
      config {
        image = "hashicorpdev/counter-dashboard:v3"
      }
    }
  }
}
