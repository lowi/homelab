variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "internal_domain" {
  default     = "internal"
  description = "The internal domain for jumpstr"
}

variable "external_domain" {
  default     = "local"
  description = "Public base URL"
}


variable "namecheap_secret_path" {
  description = "Vault path to the namcheap credentials"
  default     = "secret/data/namecheap"
}

job "ddns" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = "mini"
  }

  group "ddns" {
    network {
      mode = "bridge"
      port "http" {
        to = 8000
      }
      port "health" {
        to = 9999
      }
    }
    service {
      name = "ddns"
      port = "http"
      task = "ddns"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.ddns.entrypoints=web",
        "traefik.http.routers.ddns.rule=Host(`ddns.${var.internal_domain}`)",
        "traefik.http.routers.ddns.service=ddns",
        "traefik.http.services.ddns.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]

      connect {
        sidecar_service {}
      }
    }

    task "ddns" {
      driver = "docker"

      vault {
        policies = ["access-kv"]
      }


      config {
        image = "ghcr.io/qdm12/ddns-updater"
        ports = ["http", "health"]
      }

      env {
        LISTENING_PORT = NOMAD_PORT_http
        TZ             = "Europe/Vienna"
      }

      template {
        data        = <<EOF
CONFIG='{ "settings": [{ "host": "@", "provider_ip": true, "provider": "namecheap","domain": "${var.external_domain}", "password": "{{with secret "${var.namecheap_secret_path}"}}{{.Data.data.ddns_password}}{{end}}" }, { "host": "*", "provider_ip": true, "provider": "namecheap","domain": "${var.external_domain}", "password": "{{with secret "${var.namecheap_secret_path}"}}{{.Data.data.ddns_password}}{{end}}" }]}'
          EOF
        destination = "secrets/namecheap"
        perms       = "600"
        change_mode = "noop"
        env         = true
      }

      // resources {
      //   cpu    = 128
      //   memory = 128
      // }
    }
  }
}