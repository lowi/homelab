variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "config_dir" {
  description = "Directory for data used by the app"
}

variable "volume_id" {
  description = "Postgres volume id"
}

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base URL"
}

job "syncthing" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = "${node.class}"
    value     = "mini"
  }

  group "syncthing" {
    network {
      mode = "bridge"
      port "ui" {
        static = 8384
      }

      port "listener" {
        static = 22000
      }

      # port "udp_listener" {
      #   static = 22000
      # }

      port "discovery" {
        static = 21027
      }
    }

    volume "syncthing" {
      type      = "host"
      read_only = false
      source    = var.volume_id
    }

    service {
      name = "syncthing"
      port = "ui"
      task = "syncthing"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.syncthing.entrypoints=web",
        "traefik.http.routers.syncthing.rule=Host(`syncthing.${var.internal_domain}`)",
        "traefik.http.routers.syncthing.service=syncthing",
        "traefik.http.services.syncthing.loadbalancer.server.port=${NOMAD_HOST_PORT_ui}",
      ]

      // connect {
      //   sidecar_service {}
      // }
    }

    task "syncthing" {
      driver = "docker"

      // We need to rund the container as root,
      // as otherwise we cannot write to the volume_mount
      user = "root"

      volume_mount {
        volume      = "syncthing"
        destination = "/data"
        read_only   = false
      }

      config {
        image = "syncthing/syncthing:latest"
        ports = [ "ui", "listener", "discovery" ]
        volumes = [
          "${ var.config_dir }:/var/syncthing",
        ]
      }
      // resources {
      //   cpu    = 128
      //   memory = 128
      // }
    }
  }
}
