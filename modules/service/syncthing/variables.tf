
variable "config_dir" {
  description = "Config directory"
}

variable "volume_id" {
  description = "The id of the host volume"
}