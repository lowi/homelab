terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "syncthing" {
  jobspec = file("${path.module}/syncthing.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.namespace
      config_dir      = var.config_dir
      volume_id       = var.volume_id
      internal_domain = var.internal_domain
    }
  }
}
