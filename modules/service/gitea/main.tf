terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "gitea" {
  jobspec = file("${path.module}/gitea.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.namespace
      data_volume     = var.data_volume
      external_domain = var.external_domain
    }
  }
}
