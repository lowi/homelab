variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "external_domain" {
  default     = "internal"
  description = "The internal domain for jumpstr"
}

variable "data_volume" {
  description = "Directory for data used by the app"
}

job "gitea" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"


  constraint {
    attribute = "${meta.availability}"
    operator  = "=="
    value     = "persistent"
  }

  group "gitea" {
    network {
      mode = "bridge"
      port "http" {
        to = 3000
      }
      port "http" {
        to = 3000
      }
    }

    service {
      name = "gitea"
      port = "http"
      task = "gitea"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.gitea.entrypoints=websecure",
        "traefik.http.routers.gitea.rule=Host(`git.${var.external_domain}`)",
        "traefik.http.routers.gitea.service=gitea",
        "traefik.http.routers.gitea.tls=true",

        "traefik.http.services.gitea.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]

      connect {
        sidecar_service {}
      }
    }

    task "gitea" {
      driver = "docker"
      config {
        image = "gitea/gitea:1.20.2"
        ports = ["http"]
        volumes = [
          "local/settings.yaml:/app/config/settings.yaml",
          "local/services.yaml:/app/config/services.yaml",
          "local/widgets.yaml:/app/config/widgets.yaml",

        ]
      }

      // Render the static config from Consul KV
      template {
        destination = "local/settings.yaml"
        change_mode = "restart"
        data        = <<EOF
{{ key "${var.settings_path}" }}
EOF
      }

      template {
        destination = "local/widgets.yaml"
        change_mode = "restart"
        data        = <<EOF
{{ key "${var.widgets_path}" }}
EOF
      }

      template {
        data        = <<EOF
- Services:
{{ range services }}
  {{ if .Tags | contains "gitea.enable=true" }}
    {{ if not (.Name | regexMatch ".*sidecar-proxy$") }}
      {{ range service .Name }}
  - {{ .Name }}:
      href: http://{{ .Address}}:{{ .Port }}
        {{ range .Tags }}
          {{ if . | regexMatch "gitea.icon=.*$" }}
      icon: {{ . | replaceAll "gitea.icon=" "" }}
          {{ end }}
          {{ if . | regexMatch "gitea.description=.*$" }}
      description: {{ . | replaceAll "gitea.description=" "" }}
          {{ end }}
        {{ end }}
      {{ end }}
    {{ end }}
  {{ end }}
{{ end }}

EOF
        destination = "local/services.yaml"
      }
    }
  }
}
