variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "config_folder" {
  description = "Base directory for configuration and app data"
  default     = "/opt/example"
}

variable "data_folder" {
  description = "Base directory for configuration and app data"
  default     = "/opt/example"
}

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base domain"
}

variable "web_password" {
  description = "InternalPassword for web admin access"
}


job "pihole" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = "mini"
  }

  group "🕳" {
    network {
      mode = "host"
      port "dns" {
        static = 53
        to = 53
      }
      port "http" {
        static = 7280
        to = 80
      }
      dns {
        servers = [
          # "127.0.0.1",
          "172.17.0.1",
          "1.1.1.1",
        ]
      }
    }

    volume "pihole" {
      type      = "host"
      read_only = false
      source    = "pihole"
    }

    service {
      name = "pihole"
      port = "http"
      task = "pihole"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.pihole.entrypoints=web",
        "traefik.http.routers.pihole.rule=Host(`pihole.${var.internal_domain}`)",
        "traefik.http.services.pihole.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]
    }

    task "pihole" {
      driver = "docker"
      
      // We need to rund the container as root,
      // as otherwise we cannot write to the volume_mount
      # user = "root"

      config {
        image = "pihole/pihole:latest"
        ports = ["http", "dns"]
        volumes = [
          # "${var.config_folder}/pihole:/etc/pihole",
          # "${var.config_folder}/dnsmasq:/etc/dnsmasq.d",
        ]
      }

      volume_mount {
        volume      = "pihole"
        destination = "/etc/pihole"
        read_only   = false
      }

      env = {
        TZ          = "Europe/Vienna"
        WEBPASSWORD = var.web_password
        # DNSMASQ_USER = "root"
      }
    }
  }
}
