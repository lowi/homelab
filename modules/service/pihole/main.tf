terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "pihole" {
  jobspec = file("${path.module}/templates/pihole.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.namespace
      internal_domain = var.internal_domain
      config_folder   = var.pihole_base_folder
      web_password    = var.pihole_web_password
    }
  }
}
