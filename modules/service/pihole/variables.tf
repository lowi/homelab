variable "pihole_base_folder" {
  description = "Base folder for the pihole configuration"
  default = "/opt/pihole"
}

variable "pihole_web_password" {
  description = "Password for web login."
}