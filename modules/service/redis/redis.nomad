variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "config_path" {
  default     = "config/redis"
  description = "Consul-KV path to the redis configuration"
}

// variable "data_dir" {
//   description = "Directory for data used by the app"
// }

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base URL"
}

job "redis" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = "${meta.availability}"
    operator  = "=="
    value     = "persistent"
  }

  group "redis" {
    network {
      mode = "bridge"
      port "redis" {}
    }
    service {
      name = "redis"
      port = "redis"
      task = "redis"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.redis.entrypoints=web",
        "traefik.http.routers.redis.rule=Host(`redis.${var.internal_domain}`)",
        "traefik.http.routers.redis.service=redis",
        "traefik.http.services.redis.loadbalancer.server.port=${NOMAD_HOST_PORT_redis}",
      ]

      connect {
        sidecar_service {}
      }
    }

    task "redis" {
      driver = "docker"


      config {
        image   = "bitnami/redis:latest"
        ports   = ["redis"]
        volumes = [
          "local/config.conf:/etc/redis/redis.conf",
        ]
      }

      env {
        ALLOW_EMPTY_PASSWORD = "yes"
        REDIS_PORT_NUMBER    = NOMAD_PORT_redis
      }



      template {
        destination = "local/config.conf"
        change_mode = "restart"
        data        = <<EOF
{{ key "${var.config_path}" }}
EOF
      }

      // resources {
      //   cpu    = 128
      //   memory = 128
      // }
    }
  }

  group "rediscommander" {
    network {
      mode = "bridge"
      port "rediscommander" {
        to = 8081
      }
    }

    // volume "redis" {
    //   type      = "host"
    //   read_only = false
    //   source    = "redis"
    // }

    service {
      name = "rediscommander"
      port = "rediscommander"
      task = "rediscommander"

      tags = [
        "homepage.enable=true",
        "homepage.icon=https://cdn4.iconfinder.com/data/icons/redis-2/1451/Untitled-2-512.png",
        "homepage.description=in-memory data structure store",

        "traefik.enable=true",
        "traefik.http.routers.rediscommander.entrypoints=web",
        "traefik.http.routers.rediscommander.rule=Host(`rediscommander.${var.internal_domain}`)",
        "traefik.http.routers.rediscommander.service=rediscommander",
        "traefik.http.services.rediscommander.loadbalancer.server.port=${NOMAD_HOST_PORT_rediscommander}",
      ]

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "redis"
              local_bind_port  = 6379
            }
          }
        }
      }
    }

    task "rediscommander" {
      driver = "docker"

      env {
        // REDIS_HOSTS = "local:redis:${NOMAD_HOST_PORT_redis}"
        // HTTP_USER = "root"
        // HTTP_PASSWORD = "qwerty"
      }

      config {
        image = "rediscommander/redis-commander:latest"
        ports = ["rediscommander"]
      }
    }
  }
}