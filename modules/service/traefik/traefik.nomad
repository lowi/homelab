variable "region" {
  default = "global"
}

variable "datacenter" {
  description = "Data Center that the job is running in"
}

variable "external_domain" {
  description = "The public base URL"
}

variable "static_config_kv_path" {
  default     = "traefik/static_config"
  description = "Consul-KV path to the static configuration"
}

variable "dynamic_config_kv_path" {
  default     = "traefik/dynamic_config"
  description = "Consul-KV path to the dynamic configuration"
}

variable "namecheap_secret_path" {
  description = "Vault path to the namcheap credentials"
  default     = "secret/data/namecheap"
}

variable "acme_secret_path" {
  description = "Vault path to the namcheap credentials"
  default     = "secret/data/acme"
}

variable "http_port" {
  description = "The port bound to HTTP traffic"
  default     = "80"
}

variable "https_port" {
  description = "The port bound to HTTPS traffic"
  default     = "443"
}

variable "api_port" {
  description = "The port bound to the dashboard endpoint"
  default     = "8080"
}

variable "metrics_port" {
  description = "The port bound to Prometheus metrics"
  default     = "8082"
}

// variable "acme_json" {
//   description = "The contents of our letsencrypt json file"
//   type = string
// }

job "traefik" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = "mini"
  }

  group "traefik" {
    count = 1

    network {
      mode = "bridge"
      port "http" {
        static = var.http_port
      }
      port "https" {
        static = var.https_port
      }
      port "api" {
        static = var.api_port
      }
      port "metrics" {
        static = var.metrics_port
      }
    }

    service {
      name = "traefik"
      port = "api"
      task = "traefik"
      check {
        name     = "alive"
        type     = "tcp"
        port     = "http"
        path     = "/ping"
        interval = "10s"
        timeout  = "2s"
      }
      tags = [
        "homepage.enable=true",
        "homepage.icon=https://github.com/traefik/traefik/blob/v2.10.4/docs/content/assets/img/traefik.logo.png?raw=true",
        "homepage.description=A HTTP reverse proxy and load balancer",
      ]
      connect {
        native = true
      }
    }

    // service {
    //   name = "traefik-metrics"
    //   port = "metrics"
    //   check {
    //     type     = "tcp"
    //     interval = "10s"
    //     timeout  = "5s"
    //   }
    //   connect {
    //     native = true
    //   }
    // }

    task "traefik" {
      driver = "docker"

      vault {
        policies = ["access-kv"]
      }

      config {
        image = "traefik:2.10.4"
        ports = [
          "http",
          "https",
          "api",
          "metrics",
        ]

        /*
          "If you set the group network.mode = "bridge" you should not
            set the Docker config network_mode, or the container will be unable
            to reach other containers in the task group. This will also prevent
            Connect-enabled tasks from reaching the Envoy sidecar proxy."
          https://www.nomadproject.io/docs/drivers/docker#network_mode
        */
        // ! do NOT set the network_mode

        volumes = [
          "local/static.yaml:/etc/traefik/traefik.yaml",
          "local/dynamic.yaml:/dynamic.yaml",

          // ! If we link the acme.json file, it will try to 
          // ! sync with vault (see below: `template: acme_secret_path`)
          // * But this will render the acme.JSON (which is stored inside Vault as JSON)
          // * as HCL, which is -of course- invalid JSON
          // ? how to render the acme.json file as JSON?
          "local/acme.json:/acme.json",
        ]
      }

      // * Read the dynamic configuration from ConsulKV
      // * key, as in `{{ key "...` tells Nomad to retrieve the value from ConsulKV
      template {
        destination = "local/dynamic.yaml"
        change_mode = "restart"
        data        = <<EOF
{{ key "${var.dynamic_config_kv_path}" }}
EOF
      }

      // * Read the static configuration from ConsulKV
      template {
        destination = "local/static.yaml"
        change_mode = "restart"
        data        = <<EOF
{{ key "${var.static_config_kv_path}" }}
EOF
      }


      // * Read the Namecheap (our DNS provider) credentials from Vault 
      // * Traefik will use these to update DNS records for LetsEncrypt
      template {
        data        = <<EOF
NAMECHEAP_API_USER = {{with secret "${var.namecheap_secret_path}"}}{{.Data.data.user}}{{end}}
NAMECHEAP_API_KEY  = {{with secret "${var.namecheap_secret_path}"}}{{.Data.data.api_key}}{{end}}
EOF
        destination = "secrets/namecheap"
        perms       = "600"
        change_mode = "noop"
        env         = true
      }

//       template {
//         data        = <<EOF
// {{with secret "${var.acme_secret_path}"}}{{.Data.data}}{{end}}
// EOF
//         destination = "local/acme.json"
//         perms       = "600"
//         change_mode = "noop"
//       }
    }

    // task "cert_dumper" {
    //   driver = "docker"
    //   config {
    //     image = "ldez/traefik-certs-dumper:v2.8.1"
    //     entrypoint = [
    //       "/bin/sh -c '",
    //       "apk add jq",
    //       "; while ! [ -e /data/acme.json ]",
    //       "|| ! [ `jq '.[] | .Certificates | length' /data/acme.json` != 0 ]; do",
    //       "sleep 1",
    //       "; done",
    //       "&& traefik-certs-dumper file",
    //       "  --version v2",
    //       "  --watch",
    //       "  --domain-subdir",
    //       "  --source /data/acme.json",
    //       "  --dest /certificates'",
    //     ]
    //     volumes = [
    //       "local/certificates:/certificates",
    //     ]
    //   }
    //   lifecycle {
    //     sidecar = true
    //   }
    // }
  }
}
