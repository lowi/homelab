
variable "http_port" {
  description = "The port bound to HTTP traffic"
  type        = number
  default     = 80
}

variable "https_port" {
  description = "The port bound to HTTPS traffic"
  type        = number
  default     = 443
}

# variable "wss_unreal_port" {
#   description = "Secure WebSocket port for Unreal engines"
#   type        = number
#   default     = 8444
# }

# variable "wss_player_port" {
#   description = "Secure WebSocket port for players"
#   type        = number
#   default     = 8443
# }

variable "api_port" {
  description = "The port bound to the API endpoint"
  type        = number
  default     = 8080
}

variable "metrics_port" {
  description = "The port bound to the Prometheus exporter"
  type        = number
  default     = 8082
}
