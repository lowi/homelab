terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

locals {
  volume_id = uuidv5("dns", "${var.gluster_volume_name}.kadalu.io")
  // gluster_private_key = file("${pathexpand("${var.gluster_private_key_path}")}")
}

data "template_file" "volume_config" {
  template = file("${path.module}/volume_config.json")
  vars = {
    volume_name         = var.gluster_volume_name
    volume_id           = "be41cf8e-b62a-4ecc-96c0-60c4b475bace"
    gluster_hosts       = var.gluster_hosts
    gluster_volume_name = var.gluster_brick_name
  }
}

resource "nomad_job" "csi_controller" {
  jobspec = file("${path.module}/controller.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter     = var.datacenter,
      volume_name    = var.gluster_volume_name,
      volume_config  = data.template_file.volume_config.rendered,
      kadalu_version = var.csi_version,
    }
  }
}

resource "nomad_job" "csi_nodeplugin" {
  jobspec = file("${path.module}/node.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter     = var.datacenter,
      volume_name    = var.gluster_volume_name,
      volume_config  = data.template_file.volume_config.rendered,
      kadalu_version = var.csi_version,
    }
  }
}

// # Wait for the csi plugin to be available
// data "nomad_plugin" "csi" {
//   plugin_id        = "kadalu-csi"
//   wait_for_healthy = true
// }

