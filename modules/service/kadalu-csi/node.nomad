# Please refer 'controller.nomad' file for  variable and job descriptions
variable "datacenter" {
  default = "jumpstr"
}

variable "volume_name" {
  default = "vol1"
}

variable "volume_config" {
  description = "Gluster volume configuration file"
}

variable "kadalu_version" {
  default = "0.8.6"
}

job "csi-nodeplugin" {
  datacenters = ["${var.datacenter}"]

  # Should be running on every nomad client
  type = "system"

  update {
    stagger      = "5s"
    max_parallel = 1
  }

  group "nodeplugin" {
    task "kadalu-nodeplugin" {
      driver = "docker"

      template {
        data        = var.volume_config
        destination = "${NOMAD_TASK_DIR}/${var.volume_name}.info"
        change_mode = "noop"
      }

      template {
        data        = "${uuidv5("dns", "kadalu.io")}"
        destination = "${NOMAD_TASK_DIR}/uid"
        change_mode = "noop"
      }

      template {
        data = <<-EOS
NODE_ID        = "${node.unique.name}"
CSI_ENDPOINT   = "unix://csi/csi.sock"
KADALU_VERSION = "${var.kadalu_version}"
CSI_ROLE       = "nodeplugin"
VERBOSE        = "yes"
        EOS

        destination = "${NOMAD_TASK_DIR}/file.env"
        env         = true
      }

      template {
        data        = <<-EOS

        EOS
        destination = "${NOMAD_TASK_DIR}/..data"
        change_mode = "noop"
      }

      config {
        image = "docker.io/kadalu/kadalu-csi:${var.kadalu_version}"

        privileged = true

        mount {
          type     = "bind"
          source   = "./${NOMAD_TASK_DIR}/${var.volume_name}.info"
          target   = "/var/lib/gluster/${var.volume_name}.info"
          readonly = true
        }

        mount {
          type     = "bind"
          source   = "./${NOMAD_TASK_DIR}/uid"
          target   = "/var/lib/gluster/uid"
          readonly = true
        }

        mount {
          # Kadulu complains when ../data doesn't exist
          # I do not know what it is for.
          type     = "bind"
          source   = "./${NOMAD_TASK_DIR}/..data"
          target   = "/var/lib/gluster/..data"
          readonly = true
        }

        mount {
          type     = "tmpfs"
          target   = "/var/log/gluster"
          readonly = false

          tmpfs_options {
            size = 1000000
          }
        }
      }

      csi_plugin {
        id        = "kadalu-csi"
        type      = "node"
        mount_dir = "/csi"
      }
    }
  }
}
