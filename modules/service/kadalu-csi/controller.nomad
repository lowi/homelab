variable "datacenter" {
    default     = "jumpstr"
    description = "The datacenter the controller job runs in."
  }

  variable "volume_name" {
    default     = "nomad-pool"
    description = "Volume name for Kadalu CSI which is used for all PVC creations purposes"
  }


  variable "volume_config" {
    description = "Gluster volume configuration file"
  }

  variable "gluster_hosts" {
    default = "mini"

    description = <<-EOS
      - External gluster host where the gluster volume is created, started and quota is set
      - Multiple hosts can be supplied like "host1,host2,host3" (no spaces and trimmed endings)
      - Prefer to supply only one or else need to supply the same wherever interpolation is not supported (ex: in volume.hcl files)
      EOS
  }

  variable "kadalu_version" {
    default     = "0.8.6"
    description = "Kadalu CSI version which is tested against Nomad (v1.1.4)"
  }


  job "csi-controller" {
    datacenters = ["${var.datacenter}"]
    type        = "service"

    group "controller" {
      task "kadalu-controller" {
        driver = "docker"

        template {
          # This is basically a JSON file which is used to connect to external gluster
          # Make sure it follows JSON convention (No comma ',' for last key pair)
          data = var.volume_config
          destination = "${NOMAD_TASK_DIR}/${var.volume_name}.info"
          change_mode = "noop"
        }

        template {
          data        = "${uuidv5("dns", "kadalu.io")}"
          destination = "${NOMAD_TASK_DIR}/uid"
          change_mode = "noop"
        }

        template {
          data = <<-EOS

          EOS
          destination = "${NOMAD_TASK_DIR}/..data"
          change_mode = "noop"
        }


        template {
          # No need to supply  'SECRET_XXX' key if not using gluster native quota
          data = <<-EOS
          NODE_ID        = "${node.unique.name}"
          CSI_ENDPOINT   = "unix://csi/csi.sock"
          KADALU_VERSION = "${var.kadalu_version}"
          CSI_ROLE       = "controller"
          VERBOSE        = "yes"
          EOS

          destination = "${NOMAD_TASK_DIR}/file.env"
          env         = true
        }

        config {
          image = "docker.io/kadalu/kadalu-csi:${var.kadalu_version}"

          # Nomad client config for docker plugin should have privileged set to 'true'
          # refer https://www.nomadproject.io/docs/drivers/docker#privileged
          # Need to access '/dev/fuse' for mounting external gluster volume
          privileged = true

          mount {
            # Analogous to kadalu-info configmap
            type = "bind"

            # Make sure the source paths starts with current dir (basically: "./")
            source = "./${NOMAD_TASK_DIR}/${var.volume_name}.info"

            target   = "/var/lib/gluster/${var.volume_name}.info"
            readonly = true
          }

          mount {
            # Extra baggage for now, will be taken care in Kadalu in next release
            type     = "bind"
            source   = "./${NOMAD_TASK_DIR}/uid"
            target   = "/var/lib/gluster/uid"
            readonly = true
          }

          mount {
            # Kadulu complains when ../data doesn't exist
            # I do not know what it is for.
            type     = "bind"
            source   = "./${NOMAD_TASK_DIR}/..data"
            target   = "/var/lib/gluster/..data"
            readonly = true
          }

          mount {
            # Logging
            type     = "tmpfs"
            target   = "/var/log/gluster"
            readonly = false

            tmpfs_options {
              # 1 MB
              size = 1000000 # size in bytes
            }
          }
        }

        csi_plugin {
          id        = "kadalu-csi"
          type      = "controller"
          mount_dir = "/csi"
        }
      }
    }
  }
