variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

// variable "config_path" {
//   description = "Consul-KV path to the paperless configuration"
// }

variable "secret_path" {
  description = "Vault path to the paperless credentials"
}


// variable "data_dir" {
//   description = "Directory for data used by the app"
// }

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base URL"
}

variable "external_domain" {
  default     = "local"
  description = "The public base URL"
}

job "paperless" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = "${node.class}"
    value     = "lenovo"
  }

  group "paperless" {
    network {
      mode = "bridge"
      port "paperless" {
        to = 8000
      }
      port "gotenberg" {
        to = 3000
      }
      port "tika" {
        to = 9998
      }
    }

    service {
      name = "gotenberg"
      port = "gotenberg"
      task = "gotenberg"

      connect {
        sidecar_service {
        }
      }
    }

    service {
      name = "tika"
      port = "tika"
      task = "tika"

      connect {
        sidecar_service {
        }
      }
    }

    service {
      name = "paperless"
      port = "paperless"
      task = "paperless"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.paperless.entrypoints=web",
        "traefik.http.routers.paperless.rule=Host(`paperless.${var.internal_domain}`)",
        "traefik.http.routers.paperless.service=paperless",
        "traefik.http.services.paperless.loadbalancer.server.port=${NOMAD_HOST_PORT_paperless}",

        "homepage.enable=true",
        "homepage.icon=https://camo.githubusercontent.com/40293f8125fee61ff5edd6b4023c5723f8870f1e0fef8654a53fee0e58277dbb/68747470733a2f2f692e696d6775722e636f6d2f4635474e4b34582e706e67",
        "homepage.description=document management system",
      ]

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "redis"
              local_bind_port  = 6378
            }
            upstreams {
              destination_name = "postgres-db"
              local_bind_port  = 5433
            }
            upstreams {
              destination_name = "gotenberg"
              local_bind_port  = 13000
            }
            upstreams {
              destination_name = "tika"
              local_bind_port  = 9988
            }
          }
        }
      }
    }


    task "paperless" {
      driver = "docker"

      vault {
        policies = ["access-kv"]
      }

      config {
        image = "ghcr.io/paperless-ngx/paperless-ngx:latest"
        ports = ["paperless"]
        // volumes = [
        //   "local/config.conf:/etc/paperless/paperless.conf",
        // ]
      }

      env {
        PAPERLESS_REDIS                   = "redis://127.0.0.1:6378"
        PAPERLESS_DBHOST                  = "127.0.0.1"
        PAPERLESS_DBPORT                  = "5433"
        PAPERLESS_DBNAME                  = "paperless"
        PAPERLESS_TIKA_ENABLED            = 1
        PAPERLESS_TIKA_GOTENBERG_ENDPOINT = "http://127.0.0.1:13000"
        PAPERLESS_TIKA_ENDPOINT           = "http://127.0.0.1:9988"
        PAPERLESS_ADMIN_MAIL              = "me@${var.external_domain}"

        # The UID and GID of the user used to run paperless in the container. Set this
        # to your UID and GID on the host so that you have write access to the
        # consumption directory.
        #USERMAP_UID=1000
        #USERMAP_GID=1000

        # Additional languages to install for text recognition, separated by a
        # whitespace. Note that this is
        # different from PAPERLESS_OCR_LANGUAGE (default=eng), which defines the
        # language used for OCR.
        PAPERLESS_OCR_LANGUAGES = "eng deu"

        # This is required if you will be exposing Paperless-ngx on a public domain
        # (if doing so please consider security measures such as reverse proxy)
        #PAPERLESS_URL=https://paperless.example.com

        # Adjust this key if you plan to make paperless available publicly. It should
        # be a very long sequence of random characters. You don't need to remember it.
      }

      template {
        data        = <<EOF
{{with secret "${var.secret_path}"}}
PAPERLESS_DBUSER="{{.Data.data.db_user}}"
PAPERLESS_DBPASS="{{.Data.data.db_user}}"
PAPERLESS_ADMIN_USER="{{.Data.data.admin_user}}"
PAPERLESS_ADMIN_PASSWORD="{{.Data.data.admin_password}}"
PAPERLESS_SECRET_KEY="{{.Data.data.admin_password}}"
{{end}}
EOF
        destination = "local/credentials"
        perms       = "600"
        change_mode = "noop"
        env         = true
      }


      // resources {
      //   cpu    = 512
      //   memory = 512
      // }
    }

    task "gotenberg" {
      driver = "docker"

      config {
        image   = "docker.io/gotenberg/gotenberg:7.8"
        ports   = ["gotenberg"]
        command = "gotenberg"
        args = [
          "--chromium-disable-javascript=true",
          "--chromium-allow-list=file:///tmp/.*",
        ]
      }
    }

    task "tika" {
      driver = "docker"

      config {
        image = "ghcr.io/paperless-ngx/tika:latest"
        ports = ["tika"]
      }
      // resources {
      //   cpu    = 512
      //   memory = 512
      // }
    }
  }
}