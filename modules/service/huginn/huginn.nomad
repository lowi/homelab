variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base domain"
}

variable "external_domain" {
  default     = "example.com"
  description = "Public base domain"
}

job "huginn" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = "mini"
  }

  group "huginn" {
    network {
      mode = "bridge"
      port "http" {
        static = 3000
        to     = 3000
      }
    }

    service {
      name = "huginn"
      port = "http"
      task = "huginn"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.huginn.entrypoints=web",
        "traefik.http.routers.huginn.rule=Host(`huginn.${var.internal_domain}`)",
        "traefik.http.routers.huginn.service=huginn",
        "traefik.http.services.huginn.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]

      connect {
        sidecar_service {}
      }
    }

    task "huginn" {
      driver = "docker"

      config {
        image = "ghcr.io/huginn/huginn"
        ports = [ "http" ]
      }

      env {
        APP_SECRET_TOKEN="YJ^hA*#EpnwaP^H89uk!Hct$B2RuSP4g"
        DATABASE_ADAPTER = "postgresql"
        DATABASE_HOST = "postgres-db.service.consul"
        DATABASE_PORT = 5432
        DATABASE_NAME="huginn"
        DATABASE_USERNAME="Surname8633"
        DATABASE_PASSWORD="Banister-Stir4-Directly"
        DO_NOT_CREATE_DATABASE="true"
        // HUGINN_DATABASE_NAME="huginn" 
        // HUGINN_DATABASE_USERNAME="huginn" 
        // HUGINN_DATABASE_PASSWORD="Bucked-Sandpit7-Conduit"
      }


      resources {
        cpu    = 256
        memory = 512
      }
    }
  }
}
