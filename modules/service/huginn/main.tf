terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "huginn" {
  jobspec = file("${path.module}/huginn.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.namespace
      external_domain = var.external_domain
      internal_domain = var.internal_domain
    }
  }
}
