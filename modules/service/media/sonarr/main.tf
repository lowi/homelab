terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "sonarr" {
  jobspec = file("${path.module}/sonarr.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.namespace
      node_class      = var.media_node_class
      internal_domain = var.internal_domain
      torrent_volume  = var.torrent_download_volume
      config_volume   = var.sonarr_config_volume
      media_volume    = var.media_library_volume
      puid            = var.media_puid
      pgid            = var.media_pgid
    }
  }
}
