variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "node_class" {
  description = "The node class constraint"
}

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base URL"
}

variable "torrent_volume" {
  description = "Torrent volume id"
}

variable "config_volume" {
  description = "Volume id of the media services config"
}

variable "media_volume" {
  description = "Volume id of the media library"
}

variable "puid" {
  description = "Id of the task-user"
}

variable "pgid" {
  description = "Id of the task-user group"
}


job "sonarr" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = var.node_class
  }

  group "sonarr" {

    // * Network
    // **************************************************************
    network {
      mode = "bridge"
      port "http" {
        to = 8989
      }
    }

    // * Storage
    // **************************************************************
    volume "torrents" {
      type      = "host"
      source    = var.torrent_volume
      read_only = false
    }

    volume "config" {
      type      = "host"
      source    = var.config_volume
      read_only = false
    }

    volume "media" {
      type      = "host"
      source    = var.media_volume
      read_only = false
    }


    service {
      name = "sonarr"
      port = "http"

      tags = [
        "homepage.enable=true",
        "homepage.icon=https://github.com/Sonarr/Sonarr/blob/develop/Logo/512.png?raw=true",
        "homepage.description=TV Series",

        "traefik.enable=true",
        "traefik.http.routers.sonarr.entrypoints=web",
        "traefik.http.routers.sonarr.rule=Host(`shows.media.${var.internal_domain}`)",
        "traefik.http.services.sonarr.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]


      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "transmission"
              local_bind_port = 9091
            }
            upstreams {
              destination_name = "jackett"
              local_bind_port  = 9117
            }
          }
        }
      }
    }

    task "sonarr" {
      driver = "docker"
      config {
        image = "linuxserver/sonarr"
        ports = ["http"]
      }
      env {
        TZ   = "Europe/Vienna"
        PUID = var.puid
        PGID = var.pgid
      }
      volume_mount {
        volume      = "torrents"
        destination = "/torrents"
      }
      volume_mount {
        volume      = "media"
        destination = "/media"
      }
      volume_mount {
        volume      = "config"
        destination = "/config"
      }
    }
  }
}
