variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "node_class" {
  description = "The node class constraint"
}

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base URL"
}

variable "downloads_volume" {
  description = "Volume id for downloads"
}

variable "watch_volume" {
  description = "Volume id for watch"
}

variable "config_volume" {
  description = "Volume id of the config"
}

variable "transmission_secret_path" {
  description = "Vault path to the transmission credentials"
  default     = "secret/data/transmission"
}

variable "puid" {
  description = "Id of the task-user"
}

variable "pgid" {
  description = "Id of the task-user group"
}


job "transmission" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = var.node_class
  }

  group "transmission" {
    network {
      mode = "bridge"
      port "http" {
        static = 9091
        to     = 9091
      }
      port "control" {}
    }

    volume "downloads" {
      type      = "host"
      source    = var.downloads_volume
      read_only = false
    }

    volume "watch" {
      type      = "host"
      source    = var.watch_volume
      read_only = false
    }

    volume "config" {
      type      = "host"
      source    = var.config_volume
      read_only = false
    }

    service {
      name = "transmission"
      port = "http"

      tags = [
        "homepage.enable=true",
        "homepage.icon=https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Transmission_Icon.svg/96px-Transmission_Icon.svg.png?20110620233845",
        "homepage.description=Bittorrent client",

        "traefik.enable=true",
        "traefik.http.routers.transmission.entrypoints=web",
        "traefik.http.routers.transmission.rule=Host(`transmission.${var.internal_domain}`)",
        "traefik.http.routers.transmission.service=transmission",
        "traefik.http.services.transmission.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]

      connect {
        sidecar_service {}
      }
    }

    task "transmission" {
      driver = "docker"
      vault {
        policies = ["access-kv"]
      }
      config {
        image = "linuxserver/transmission"
        ports = ["http", "control"]
      }
      env {
        PEERPORT = "${NOMAD_PORT_control}"
        TZ       = "Europe/Vienna"
        PUID     = var.puid
        PGID     = var.pgid
      }
      volume_mount {
        volume      = "downloads"
        destination = "/downloads"
      }
      volume_mount {
        volume      = "watch"
        destination = "/watch"
      }
      volume_mount {
        volume      = "config"
        destination = "/config"
      }
      template {
        data        = <<EOF
USER={{with secret "${var.transmission_secret_path}"}}{{.Data.data.username}}{{end}}
PASS={{with secret "${var.transmission_secret_path}"}}{{.Data.data.password}}{{end}}
EOF
        destination = "local/credentials"
        perms       = "600"
        change_mode = "restart"
        env         = true
      }

      // resources {
      //   cpu    = 128
      //   memory = 128
      // }
    }
  }
}
