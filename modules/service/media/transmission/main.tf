terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "transmission" {
  jobspec = file("${path.module}/transmission.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter       = var.datacenter
      region           = var.namespace
      node_class       = var.media_node_class
      internal_domain  = var.internal_domain
      config_volume    = var.transmission_config_volume
      downloads_volume = var.torrent_download_volume
      watch_volume     = var.torrent_watch_volume
      puid             = var.media_puid
      pgid             = var.media_pgid
    }
  }
}
