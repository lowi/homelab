variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "node_class" {
  description = "The node class constraint"
}

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base URL"
}

variable "torrent_volume" {
  description = "Torrent volume id"
}

variable "config_volume" {
  description = "Volume id of the media services config"
}

variable "media_volume" {
  description = "Volume id of the media library"
}

variable "puid" {
  description = "Id of the task-user"
}

variable "pgid" {
  description = "Id of the task-user group"
}

job "lidarr" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = var.node_class
  }

  group "lidarr" {

    // * Network
    // **************************************************************
    network {
      mode = "bridge"
      port "http" {
        to = 8686
      }
    }

    // * Storage
    // **************************************************************
    volume "torrents" {
      type      = "host"
      source    = var.torrent_volume
      read_only = false
    }

    volume "config" {
      type      = "host"
      source    = var.config_volume
      read_only = false
    }

    volume "media" {
      type      = "host"
      source    = var.media_volume
      read_only = false
    }


    service {
      name = "lidarr"
      port = "http"

      tags = [
        "homepage.enable=true",
        "homepage.icon=https://avatars.githubusercontent.com/u/28475832?s=200&v=4",
        "homepage.description=Music",

        "traefik.enable=true",
        "traefik.http.routers.lidarr.entrypoints=web",
        "traefik.http.routers.lidarr.rule=Host(`music.media.${var.internal_domain}`)",
        "traefik.http.services.lidarr.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]


      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "transmission"
              # * We need some port that is not used by other services
              local_bind_port = 9091
            }
            upstreams {
              destination_name = "jackett"
              local_bind_port  = 9117
            }
          }
        }
      }
    }

    task "lidarr" {
      driver = "docker"
      config {
        image = "linuxserver/lidarr"
        ports = ["http"]
      }
      env {
        TZ   = "Europe/Vienna"
        PUID = var.puid
        PGID = var.pgid
      }
      volume_mount {
        volume      = "torrents"
        destination = "/torrents"
      }
      volume_mount {
        volume      = "media"
        destination = "/media"
      }
      volume_mount {
        volume      = "config"
        destination = "/config"
      }
      // resources {
      //   cpu    = 4096
      //   memory = 4096
      // }
    }
  }
}
