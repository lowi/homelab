variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "node_class" {
  description = "The node class constraint"
}

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base URL"
}

variable "torrent_volume" {
  description = "Torrent volume id"
}

variable "config_volume" {
  description = "Volume id of the media services config"
}


job "jackett" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = var.node_class
  }

  group "jackett" {

    // * Network
    // **************************************************************
    network {
      mode = "bridge"
      port "http" {
        to = 9117
      }
    }

    // * Storage
    // **************************************************************
    volume "torrents" {
      type      = "host"
      source    = var.torrent_volume
      read_only = false
    }

    volume "config" {
      type      = "host"
      source    = var.config_volume
      read_only = false
    }

    service {
      name = "jackett"
      port = "http"
      task = "jackett"

      tags = [
        "homepage.enable=true",
        "homepage.icon=https://github.com/Jackett/Jackett/blob/master/src/Jackett.Common/Content/jacket_medium.png?raw=true",
        "homepage.description=Torrent trackers",
        "traefik.enable=true",
        "traefik.http.routers.jackett.entrypoints=web",
        "traefik.http.routers.jackett.rule=Host(`jackett.media.${var.internal_domain}`)",
        "traefik.http.services.jackett.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]

      connect {
        sidecar_service {
          proxy {}
        }
      }
    }

    task "jackett" {
      driver = "docker"
      config {
        image = "linuxserver/jackett"
        ports = ["http"]
      }
      volume_mount {
        volume      = "torrents"
        destination = "/downloads"
      }
      volume_mount {
        volume      = "config"
        destination = "/config"
      }
    }
  }
}
