variable "region" {
  default = "global"
}

variable "datacenter" {
  description = "Data Center that the job is running in"
}

variable "node_class" {
  description = "The node class constraint"
}

variable "internal_domain" {
  description = "Internal LAN base URL"
}

variable "media_volume" {
  description = "Volume id of the media library"
}

variable "config_volume" {
  description = "Volume id of the media services config"
}


job "calibre" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = var.node_class
  }

  group "calibre" {

    // * Network
    // **************************************************************
    network {
      mode = "bridge"
      port "desktop" {
        to = 8080
      }
      port "webserver" {
        to = 8081
      }
    }

    // * Storage
    // **************************************************************
    volume "media" {
      type      = "host"
      source    = var.media_volume
      read_only = false
    }

    volume "config" {
      type      = "host"
      source    = var.config_volume
      read_only = false
    }

    service {
      name = "calibre"
      port = "desktop"
      task = "calibre"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.calibre.entrypoints=web",
        "traefik.http.routers.calibre.rule=Host(`calibre.${var.internal_domain}`)",
        "traefik.http.services.calibre.loadbalancer.server.port=${NOMAD_HOST_PORT_desktop}",
      ]

      connect {
        sidecar_service {
          proxy {}
        }
      }
    }

    task "calibre" {
      driver = "docker"
      config {
        image = "linuxserver/calibre"
        ports = ["webserver", "desktop"]
      }
      volume_mount {
        volume      = "media"
        destination = "/media"
      }
      volume_mount {
        volume      = "config"
        destination = "/config"
      }
      // resources {
      //   cpu    = 512
      //   memory = 512
      // }
    }
  }
}
