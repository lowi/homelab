variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "node_class" {
  description = "The node class constraint"
}

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base URL"
}

variable "media_volume" {
  description = "Volume id of the media library"
}

variable "config_volume" {
  description = "Volume id of the media services config"
}


job "jellyfin" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = var.node_class
  }

  group "jellyfin" {

    // * Network
    // **************************************************************
    network {
      mode = "bridge"
      port "http" {
        to = 8096
      }
    }

    // * Storage
    // **************************************************************
    volume "media" {
      type      = "host"
      source    = var.media_volume
      read_only = false
    }

    volume "config" {
      type      = "host"
      source    = var.config_volume
      read_only = false
    }

    service {
      name = "jellyfin"
      port = "http"
      task = "jellyfin"

      tags = [
        "homepage.enable=true",
        "homepage.icon=https://user-images.githubusercontent.com/10274099/89431928-d20a7180-d740-11ea-9efd-3941dcac48ef.png",
        "homepage.description=Media library",
        "traefik.enable=true",
        "traefik.http.routers.jellyfin.entrypoints=web",
        "traefik.http.routers.jellyfin.rule=Host(`flix.${var.internal_domain}`)",
        "traefik.http.services.jellyfin.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]

      connect {
        sidecar_service {}
      }
    }

    task "jellyfin" {
      driver = "docker"
      config {
        image = "jellyfin/jellyfin"
        ports = ["http"]
      }
      volume_mount {
        volume      = "media"
        destination = "/media"
      }
      volume_mount {
        volume      = "config"
        destination = "/config"
      }
      resources {
        cpu    = 2048
        memory = 2048
      }
    }
  }
}
