terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "jackett" {
  jobspec = file("${path.module}/jellyfin.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.namespace
      node_class      = var.media_node_class
      internal_domain = var.internal_domain
      config_volume   = var.jellyfin_config_volume
      media_volume    = var.media_library_volume
    }
  }
}
