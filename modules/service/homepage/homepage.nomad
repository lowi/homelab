variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "internal_domain" {
  default     = "internal"
  description = "The internal domain for jumpstr"
}

variable "settings_path" {
  default     = "homepage/settings"
  description = "Consul-KV path to the settings"
}

variable "widgets_path" {
  default     = "homepage/widgets"
  description = "Consul-KV path to the widgets"
}

// variable "services_path" {
//   default     = "homepage/services"
//   description = "Consul-KV path to the service-definitions"
// }

variable "config_dir" {
  description = "Directory for data used by the app"
}

locals {

  // ! here we construct the categories from the services


  datacenter = "${var.datacenter}"
  # unlike variables, locals don't have type constraints, so if you use
  # functions that take maps but not objects, you may need to convert them
  number_of_ports = length(convert({ "www" = "80" }, map(string)))


}

job "homepage" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"


  constraint {
    attribute = "${meta.availability}"
    operator  = "==" # Match exactly "persistent"
    value     = "persistent"
  }

  group "homepage" {
    network {
      mode = "bridge"
      port "http" {
        to = 3000
      }
    }

    service {
      name = "homepage"
      port = "http"
      task = "homepage"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.homepage.entrypoints=web",
        "traefik.http.routers.homepage.rule=Host(`home.${var.internal_domain}`)",
        "traefik.http.routers.homepage.service=homepage",
        "traefik.http.services.homepage.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]

      connect {
        sidecar_service {}
      }
    }

    task "homepage" {
      driver = "docker"
      config {
        image = "ghcr.io/benphelps/homepage:latest"
        ports = ["http"]
        volumes = [
          "local/settings.yaml:/app/config/settings.yaml",
          "local/services.yaml:/app/config/services.yaml",
          "local/widgets.yaml:/app/config/widgets.yaml",

        ]
      }

      // Render the static config from Consul KV
      template {
        destination = "local/settings.yaml"
        change_mode = "restart"
        data        = <<EOF
{{ key "${var.settings_path}" }}
EOF
      }

      template {
        destination = "local/widgets.yaml"
        change_mode = "restart"
        data        = <<EOF
{{ key "${var.widgets_path}" }}
EOF
      }

      template {
        destination = "local/localll.yaml"
        change_mode = "noop"
        data        = <<EOF
{{ "${var.widgets_path}" }}
{{ "${local.datacenter}" }}
{{ "${local.datacenter}" }}
{{ "${local.number_of_ports}" }}
EOF
      }




      // grouped_services = {
      //     for service in var.services :
      //     service.Tags["group_key"] => service
      //   }

      // * Extract tags from the Consul service definition
      template {
        data        = <<EOF
- Services:
{{ range services }}
  {{ if .Tags | contains "homepage.enable=true" }}
    {{ if not (.Name | regexMatch ".*sidecar-proxy$") }}
      {{ range service .Name }}
  - {{ .Name }}:
      href: http://{{ .Address}}:{{ .Port }}
        {{ range .Tags }}
          {{ if . | regexMatch "homepage.icon=.*$" }}
      icon: {{ . | replaceAll "homepage.icon=" "" }}
          {{ end }}
          {{ if . | regexMatch "homepage.description=.*$" }}
      description: {{ . | replaceAll "homepage.description=" "" }}
          {{ end }}
        {{ end }}
      {{ end }}
    {{ end }}
  {{ end }}
{{ end }}

EOF
        destination = "local/services.yaml"
      }
    }
  }
}
