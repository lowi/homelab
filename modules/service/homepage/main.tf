terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "homepage" {
  jobspec = file("${path.module}/homepage.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.namespace
      config_dir      = var.config_dir
      internal_domain = var.internal_domain
    }
  }
}
