variable "location" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "volume_id" {
  description = "Postgres volume id"
}

variable "secret_path" {
  description = "Vault path for Postgres credentials"
}

job "psql" {
  region      = var.location
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    // ! fixed to mini because of the volume mount
    value = "mini"
  }

  group "psql" {
    network {
      mode = "bridge"
      port "psql" {
        to = 5432
      }
    }

    volume "psql" {
      type      = "host"
      read_only = false
      source    = var.volume_id
    }


    service {
      name = "psql"
      task = "psql"
      port = "psql"
      // check {
      //   name     = "alive"
      //   type     = "tcp"
      //   path     = "/"
      //   interval = "10s"
      //   timeout  = "2s"
      // }
      connect {
        sidecar_service {
        }
      }
    }

    restart {
      attempts = 2
      interval = "2m"
      delay    = "25s"
      mode     = "fail"
    }

    task "psql" {
      driver = "docker"

      vault {
        policies = ["access-kv"]
      }

      volume_mount {
        volume      = "psql"
        destination = "/var/lib/postgresql/data"
        read_only   = false
      }

      env {
        PGDATA      = "/var/lib/postgresql/data/pgdata"
        POSTGRES_DB = "postgres"
      }

      config {
        image = "postgres:15"
        // ulimit {
        //   memlock = "-1"
        //   nofile  = "65536"
        //   nproc   = "4096"
        // }
      }
      resources {
        cpu    = 1500
        memory = 1024
      }

      template {
        data        = <<EOF
POSTGRES_USER = "{{with secret "${var.secret_path}"}}{{.Data.data.root_user}}{{end}}"
POSTGRES_PASSWORD="{{with secret "${var.secret_path}"}}{{.Data.data.root_password}}{{end}}"
EOF
        destination = "secrets/postgres"
        perms       = "600"
        change_mode = "noop"
        env         = true
      }
    }
  }
}

