terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "postgres" {
  jobspec = file("${path.module}/postgres.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"  = var.datacenter,
      "location"    = var.datacenter,
      "volume_id"   = var.postgres_volume_id,
      "secret_path" = var.postgres_secret_path,
    }
  }
}
