variable "location" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "external_domain" {
  description = "Public base domain"
}

variable "upstream_psql" {
  description = "the name of the upstream psql service"
}

variable "upstream_mysql" {
  description = "the name of the upstream mariadb service"
}

job "adminer" {
  region      = var.location
  datacenters = [var.datacenter]
  type        = "service"

  group "adminer" {

    network {
      mode = "bridge"
      port "adminer" {
        to = 8080
      }
    }

    service {
      name = "adminer"
      task = "adminer"
      port = "adminer"
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.adminer.entrypoints=websecure",
        "traefik.http.routers.adminer.rule=Host(`db.${var.external_domain}`)",
        "traefik.http.routers.adminer.tls=true",
        "traefik.http.routers.adminer.tls.certresolver=le",
        "traefik.http.routers.adminer.service=adminer",
        "traefik.http.services.adminer.loadbalancer.server.port=${NOMAD_HOST_PORT_adminer}",

        "homepage.enable=true",
        "homepage.category=system/database",
        "homepage.icon=https://www.adminer.org/static/images/logo.png",
        "homepage.description=Database Admin UI",
      ]

      check {
        name     = "adminer ui alive"
        type     = "http"
        method   = "GET"
        path     = "/"
        interval = "10s"
        timeout  = "2s"
      }

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "psql"
              local_bind_port  = 5432
            }
            upstreams {
              destination_name = "mariadb"
              local_bind_port  = 3306
            }
          }
        }
      }
    }

    restart {
      attempts = 2
      interval = "2m"
      delay    = "25s"
      mode     = "fail"
    }

    task "adminer" {
      driver = "docker"

      env {
        // * we're connecting upstream through the consul-connect-sidecar-proxy
        // * see right above: `connect {`
        ADMINER_DEFAULT_SERVER = "0.0.0.0:15432"
        ADMINER_DESIGN         = "dracula"
      }

      config {
        image = "adminer:latest"
        ports = ["adminer"]
      }
    }
  }
}

