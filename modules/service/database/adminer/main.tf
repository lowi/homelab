terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}


resource "nomad_job" "adminer" {
  jobspec = file("${path.module}/adminer.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"      = var.datacenter,
      "location"        = var.datacenter,
      "external_domain" = var.external_domain,
      "upstream_psql"   = "psql",
      "upstream_mysql"  = "mariadb",
    }
  }
}
