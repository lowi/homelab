variable "location" {
  default = "global"
}

variable "datacenter" {
  description = "Data Center that the job is running in"
}

variable "volume_id" {
  description = "Postgres volume id"
}

variable "secret_path" {
  description = "Vault path for MaraiaBD"
}

job "mysql" {
  region      = var.location
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    // ! fixed to mini because of the volume mount
    value = "mini"
  }

  group "mariadb" {
    network {
      mode = "bridge"
      port "mariadb" {
        to = 3306
      }
    }

    volume "mariadb" {
      type      = "host"
      read_only = false
      source = var.volume_id
    }


    service {
      name = "mariadb"
      task = "mariadb"
      port = "mariadb"
      // check {
      //   name     = "alive"
      //   type     = "tcp"
      //   path     = "/"
      //   interval = "10s"
      //   timeout  = "2s"
      // }
      connect {
        sidecar_service {}
      }
    }

    restart {
      attempts = 2
      interval = "2m"
      delay    = "25s"
      mode     = "fail"
    }

    task "mariadb" {
      driver = "docker"

      volume_mount {
        volume      = "mariadb"
        destination = "/var/lib/mysql"
        read_only   = false
      }

      config {
        image = "mariadb:11"
      }

      vault {
        policies = ["access-kv"]
      }

      // resources {
      //   cpu    = 1500
      //   memory = 1024
      // }

      template {
        data        = <<EOF
MARIADB_ROOT_PASSWORD="{{with secret "${var.secret_path}"}}{{.Data.data.root_password}}{{end}}"
EOF
        destination = "secrets/mariadb"
        perms       = "600"
        change_mode = "noop"
        env         = true
      }
    }
  }
}

