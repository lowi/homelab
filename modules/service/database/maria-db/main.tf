terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "maria_db" {
  jobspec = file("${path.module}/maria-db.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"  = var.datacenter,
      "location"    = var.datacenter,
      "volume_id"   = var.mariadb_volume_id,
      "secret_path" = var.mariadb_secret_path,
    }
  }
}
