variable "datacenter" {
  default     = "jumpstr"
  description = "Data Center that the job needs to be run in"
}

variable "external_domain" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "config_kv_path" {
  default     = "vaultwarden"
  description = "Consul-KV path to the configuration"
}


job "vaultwarden" {
  datacenters = [var.datacenter]
  type        = "service"

  group "vaultwarden" {

    network {
      mode = "bridge"
      port "http" {}
      port "websocket" {}
    }

    volume "vaultwarden" {
      type      = "host"
      read_only = false
      source    = "vaultwarden"
    }


    service {
      name = "vaultwarden"
      tags = [
        "homepage.enable=true",
        "homepage.icon=https://github.com/bitwarden/mobile/blob/v2023.7.0/appIcons/iOS/prod.png?raw=true",
        "homepage.description=Bitwarden server",

        "traefik.enable=true",

        "traefik.http.routers.vaultwarden-ui.entrypoints=websecure",
        "traefik.http.routers.vaultwarden-ui.rule=Host(`keys.${var.external_domain}`)",
        "traefik.http.routers.vaultwarden-ui.service=vaultwarden-ui",
        "traefik.http.routers.vaultwarden-ui.tls=true",
        "traefik.http.routers.vaultwarden-ui.tls.certresolver=le",
        "traefik.http.routers.vaultwarden-ui.tls.domains[0].main=${var.external_domain}",
        "traefik.http.routers.vaultwarden-ui.tls.domains[0].sans=*.${var.external_domain}",
        "traefik.http.services.vaultwarden-ui.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",

        "traefik.http.routers.vaultwarden-websocket.entrypoints=websecure",
        "traefik.http.routers.vaultwarden-websocket.rule=Host(`${var.external_domain}`) && Path(`/notifications/hub`)",
        "traefik.http.routers.vaultwarden-websocket.service=vaultwarden-websocket",
        "traefik.http.routers.vaultwarden-websocket.tls=true",
        "traefik.http.routers.vaultwarden-websocket.tls.certresolver=le",
        "traefik.http.routers.vaultwarden-websocket.tls.domains[0].main=${var.external_domain}",
        "traefik.http.routers.vaultwarden-websocket.tls.domains[0].sans=*.${var.external_domain}",
        "traefik.http.services.vaultwarden-websocket.loadbalancer.server.port=${NOMAD_HOST_PORT_websocket}",
      ]

      port = "http"

      // check {
      //   name     = "vaultwarden HTTP"
      //   type     = "http"
      //   path     = "/-/healthy"
      //   interval = "10s"
      //   timeout  = "2s"
      // }

      connect {
        sidecar_service {}
      }
    }

    # restart {
    #   attempts = 3
    #   interval = "5m"
    #   delay    = "30s"
    #   mode     = "fail"
    # }

    task "vaultwarden" {
      driver = "docker"

      volume_mount {
        volume      = "vaultwarden"
        destination = "/data"
        read_only   = false
      }

      config {
        image = "vaultwarden/server:latest"
        ports = ["http", "websocket"]
      }

      env {
        DOMAIN              = "https://keys.${var.external_domain}"
        DISABLE_ADMIN_TOKEN = true
        ROCKET_PORT         = "${NOMAD_PORT_http}"
        WEBSOCKET_PORT      = "${NOMAD_PORT_websocket}"
        WEBSOCKET_ENABLED   = true
        WEB_VAULT_ENABLED   = true
      }

      // * Read the configuration from ConsulKV
      template {
        destination = "${NOMAD_TASK_DIR}/vaultwarden.env"
        change_mode = "restart"
        env         = true
        data        = <<EOF
{{ key "${var.config_kv_path}" }}
EOF   
      }

      resources {
        cpu    = 500
        memory = 128
      }
    }
  }
}
