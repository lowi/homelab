// ! Ignore.
// Just fore reference should we decide to use GlusterFS CSI driver

# Unfortunately 'variable' interpolation isn't supported in volume spec
# so, parameters has to be supplied again
id = "956d6400-6f4d-5dc5-9b33-b3c2aaa3e7b6"
name = "brick0"
type = "csi"
plugin_id = "kadalu-csi"
capacity_min = "200M"
capacity_max = "1G"

capability {
  access_mode     = "single-node-writer"
  attachment_mode = "file-system"
}

parameters {
  kadalu_format = "native"
  # Below parameters needs to be replaced correctly based on
  # json file supplied during controller/nodeplugin job
  storage_name = "nomad-pool"

  gluster_hosts   = "mini.jumpstr"
  gluster_volume_name = "brick0"

  volume_name = "brick0"
  volume_id = "brick0.mini.media-glusterfs-brick0"
}

  // /opt/sbin/glusterfs --volfile-server=mini.jumpstr --log-level=DEBUG --volfile-id=--volfile-id brick0.mini.media-glusterfs-brick0 /mnt/nomad-pool
