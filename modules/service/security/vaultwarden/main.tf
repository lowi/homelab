
// resource "nomad_external_volume" "vaultwarden" {
//   type      = "csi"
//   plugin_id = var.csi_plugin_id
//   volume_id = var.gluster_brick_name
//   name      = var.gluster_brick_name

//   capacity_min = "200M"
//   capacity_max = "1G"

//   capability {
//     access_mode     = "single-node-writer"
//     attachment_mode = "file-system"
//   }

//   parameters = {
//     kadalu_format   = "native"
//     storage_name    = var.gluster_volume_name
//     gluster_hosts   = var.gluster_hosts
//     gluster_volname = var.gluster_brick_name
//   }
// }

// resource "nomad_volume" "vaultwarden_volume" {
//   type        = "csi"
//   plugin_id   = var.csi_plugin_id
//   volume_id   = "nomadvolume"
//   name        = "nomadvolume"
//   external_id = "be41cf8e-b62a-4ecc-96c0-60c4b475bace"

//   capability {
//     access_mode     = "single-node-writer"
//     attachment_mode = "file-system"
//   }
// }

resource "nomad_job" "vaultwarden" {
  jobspec = file("${path.module}/vaultwarden.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter,
      external_domain = var.external_domain,
    }
  }
}

