variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "internal_domain" {
  default     = "local"
  description = "Internal LAN base domain"
}

variable "external_domain" {
  default     = "example.com"
  description = "Public base domain"
}

variable "config_dir" {
  description = "Directory for data used by the app"
}

job "home_assistant" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = node.class
    value     = "mini"
  }

  group "home_assistant" {
    network {
      mode = "bridge"
      port "http" {
        static = 8123
        to = 8123
      }
    }

    service {
      name = "home-assistant"
      port = "http"
      task = "home_assistant"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.home_assistant.entrypoints=websecure",
        "traefik.http.routers.home_assistant.rule=Host(`assistant.${var.external_domain}`)",
        "traefik.http.routers.home_assistant.service=home_assistant",
        "traefik.http.routers.home_assistant.tls=true",
        "traefik.http.routers.home_assistant.tls.certresolver=le",
        "traefik.http.routers.home_assistant.tls.domains[0].main=${var.external_domain}",
        "traefik.http.routers.home_assistant.tls.domains[0].sans=*.${var.external_domain}",
        "traefik.http.services.home_assistant.loadbalancer.server.port=${NOMAD_PORT_http}",
      ]

      connect {
        sidecar_service {}
      }
    }

    task "home_assistant" {
      driver = "docker"
      user = "root"
      config {
        image = "homeassistant/home-assistant"
        privileged = true
        ports = [ "http" ]
        volumes = [
          // "/etc/localtime:/etc/localtime:ro",
          "${ var.config_dir }:/config",
        ]
      }

      // resources {
      //   cpu    = 128
      //   memory = 128
      // }
    }
  }
}
