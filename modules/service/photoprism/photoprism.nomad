variable "photoprism_admin_user" {
  description = "admin login username"
  default     = "admin"
}

variable "photoprism_admin_password" {
  description = "initial admin password (8-72 characters)"
  default     = "insecure"
}

variable "photoprism_auth_mode" {
  description = "authentication mode (public, password)"
  default     = "password"
}

variable "photoprism_site_url" {
  description = "server URL in the format http(s)://domain.name(:port)/(path)"
  default     = "http://localhost:2342/"
}

variable "photoprism_disable_tls" {
  description = "disables HTTPS/TLS even if the site URL starts with https:// and a certificate is available"
  default     = "false"
}

variable "photoprism_default_tls" {
  description = "defaults to a self-signed HTTPS/TLS certificate if no other certificate is available"
  default     = "true"
}

variable "photoprism_originals_limit" {
  description = "file size limit for originals in MB (increase for high-res video)"
  default     = 5000
}

variable "photoprism_http_compression" {
  description = "improves transfer speed and bandwidth utilization (none or gzip)"
  default     = "gzip"
}

variable "photoprism_log_level" {
  description = "log level: trace, debug, info, warning, error, fatal, or panic"
  default     = "info"
}

variable "photoprism_readonly" {
  description = "do not modify originals directory (reduced functionality)"
  default     = "false"
}

variable "photoprism_experimental" {
  description = "enables experimental features"
  default     = "false"
}

variable "photoprism_disable_chown" {
  description = "disables updating storage permissions via chmod and chown on startup"
  default     = "false"
}

variable "photoprism_disable_webdav" {
  description = "disables built-in WebDAV server"
  default     = "false"
}

variable "photoprism_disable_settings" {
  description = "disables settings UI and API"
  default     = "false"
}

variable "photoprism_disable_tensorflow" {
  description = "disables all features depending on TensorFlow"
  default     = "false"
}

variable "photoprism_disable_faces" {
  description = "disables face detection and recognition (requires TensorFlow)"
  default     = "false"
}

variable "photoprism_disable_classification" {
  description = "disables image classification (requires TensorFlow)"
  default     = "false"
}

variable "photoprism_disable_vectors" {
  description = "disables vector graphics support"
  default     = "false"
}

variable "photoprism_disable_raw" {
  description = "disables indexing and conversion of RAW images"
  default     = "false"
}

variable "photoprism_raw_presets" {
  description = "enables applying user presets when converting RAW images (reduces performance)"
  default     = "false"
}

variable "photoprism_jpeg_quality" {
  description = "a higher value increases the quality and file size of JPEG images and thumbnails (25-100)"
  default     = 85
}

variable "photoprism_detect_nsfw" {
  description = "automatically flags photos as private that MAY be offensive (requires TensorFlow)"
  default     = "false"
}

variable "photoprism_upload_nsfw" {
  description = "allows uploads that MAY be offensive (no effect without TensorFlow)"
  default     = "true"
}

variable "photoprism_database_driver" {
  description = "SQLite is an embedded database that doesn't require a server"
  default     = "sqlite"
}

variable "photoprism_database_driver" {
  description = "use MariaDB 10.5+ or MySQL 8+ instead of SQLite for improved performance"
  default     = "mysql"
}

variable "photoprism_database_server" {
  description = "MariaDB or MySQL database server (hostname:port)"
  default     = "mariadb:3306"
}

variable "photoprism_database_name" {
  description = "MariaDB or MySQL database schema name"
  default     = "photoprism"
}

variable "photoprism_database_user" {
  description = "MariaDB or MySQL database user name"
  default     = "photoprism"
}

variable "photoprism_database_password" {
  description = "MariaDB or MySQL database user password"
  default     = "insecure"
}

variable "photoprism_site_caption" {
  description = "HTML Tile "
  default     = "AI-Powered Photos App"
}

variable "photoprism_site_description" {
  description = "meta site description"
  default     = ""
}

variable "photoprism_site_author" {
  description = "meta site author"
  default     = ""
}

variable "photoprism_ffmpeg_encoder" {
  description = "H.264/AVC encoder (software, intel, nvidia, apple, raspberry, or vaapi)"
  default     = "software"
}

variable "photoprism_ffmpeg_size" {
  description = "video size limit in pixels (720-7680) (default: 3840)"
  default     = "1920"
}

variable "photoprism_ffmpeg_bitrate" {
  description = "video bitrate limit in Mbit/s (default: 50)"
  default     = "32"
}

variable "photoprism_init" {
  description = "Run/install on first startup (options: update https gpu tensorflow davfs clitools clean)         "
  default     = "https gpu tensorflow"
}

variable "photoprism_uid" {
  description = "Process user ID (supported: 0, 33, 50-99, 500-600, and 900-1200):"
  default     = 1000
}

variable "photoprism_gid" {
  description = "Process user ID (supported: 0, 33, 50-99, 500-600, and 900-1200)"
  default     = 1000
}

variable "photoprism_umask" {
  description = "Process UMASK"
  default     = "0000"
}