terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "photoprism" {
  jobspec = file("${path.module}/photoprism.nomad")
  hcl2 {
    enabled = true
    vars = {
      
    }
  }
}
