

variable "media_base_folder" {
  description = "Base direcory for all media"
}

variable "movies_base_folder" {
  description = "Base direcory for movies"
}

variable "series_base_folder" {
  description = "Base directory for TV series"
}

variable "books_base_folder" {
  description = "Base directory for books"
}

variable "audiobooks_base_folder" {
  description = "Base directory for audiobooks"
}

variable "torrent_base_folder" {
  description = "Torrent base directory"
}
