terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "tiny_rss" {
  jobspec = file("${path.module}/tiny_rss.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.namespace
      internal_domain = var.internal_domain
      external_domain = var.external_domain
    }
  }
}
