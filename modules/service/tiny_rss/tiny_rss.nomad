variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "internal_domain" {
  default     = "internal"
  description = "The internal domain for jumpstr"
}

variable "external_domain" {
  default     = "local"
  description = "Public base URL"
}

variable "secret_path" {
  description = "Vault path to the namcheap credentials"
  default     = "secret/data/tiny-rss"
}

job "rss" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  constraint {
    attribute = "${meta.availability}"
    operator  = "=="
    value     = "persistent"
  }


  group "rss" {
    network {
      mode = "bridge"
      port "http" {}
    }
    service {
      name = "rss"
      port = "http"
      task = "rss"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.rss.entrypoints=websecure",
        "traefik.http.routers.rss.rule=Host(`rss.${var.external_domain}`)",
        "traefik.http.routers.rss.tls=true",
        "traefik.http.routers.rss.tls.certresolver=le",
        "traefik.http.routers.rss.service=rss",
        "traefik.http.services.rss.loadbalancer.server.port=${NOMAD_HOST_PORT_http}",
      ]

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name   = "postgres-db"
              local_bind_port    = 5432
            }
          }
        }
      }
    }

    task "rss" {
      driver = "docker"

      vault {
        policies = ["access-kv"]
      }

      config {
        image = "cthulhoo/ttrss-fpm-pgsql-static"
        ports = ["http"]
      }

      env {
        HTTP_PORT     = NOMAD_PORT_http
        TTRSS_DB_USER = "tinyrss"
        TTRSS_DB_NAME = "tinyrss"
        TTRSS_DB_HOST = "localhost"
        TTRSS_DB_PORT = 5432
      }

      template {
        data        = <<EOF
ADMIN_USER_PASS="{{with secret "${var.secret_path}"}}{{.Data.data.admin_user_pass}}{{end}}"
TTRSS_DB_PASS="{{with secret "${var.secret_path}"}}{{.Data.data.db_pass}}{{end}}"
          EOF
        destination = "secrets/tiny-rss"
        perms       = "600"
        change_mode = "noop"
        env         = true
      }

    }
  }
}