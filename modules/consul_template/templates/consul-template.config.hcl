
# Consul Template configuration
# ————————————————————————————————————————————————————————————————
# https://github.com/hashicorp/consul-template/blob/main/docs/configuration.md

vault {
  address      = "${vault_address}"
  token        = "${vault_token}"
  unwrap_token = false
  renew_token  = true

  ssl {
    enabled     = true
    verify      = true
  }
}

syslog { enabled = true }

${stanzas}
