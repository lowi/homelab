locals {

  consul_secret = "${var.intermediate_pki_path}/issue/${var.consul_role}"
  consul_name   = "client.${var.datacenter}.${var.consul_domain}"

  certificate_templates = [

    # Consul PKI
    # ————————————————————————————————————————————————————————————————
    {
      name    = "consul.ca.crt.tpl"
      dest    = "${var.certificates_base_folder}/consul/ca.crt"
      cmd     = "echo 'update: consul/ca.crt' && sighup-consul"
      content = <<EOT
{{ with secret "${local.consul_secret}" "common_name=${local.consul_name}" "ttl=730h"}}
{{ .Data.issuing_ca }}
{{ end }}
EOT
    },
    # https://github.com/hashicorp/consul/issues/7926#issuecomment-688042216
    {
      name    = "consul.ca_chain.crt.tpl"
      dest    = "${var.certificates_base_folder}/consul/ca_chain.crt"
      cmd     = "echo 'update: consul/ca_chain.crt' && sighup-consul"
      content = <<EOT
{{ with secret "${var.intermediate_pki_path}/cert/ca_chain" }}
{{ .Data.ca_chain }}
{{ end }}
EOT
    },
    {
      name    = "consul.agent.crt.tpl"
      dest    = "${var.certificates_base_folder}/consul/agent.crt"
      cmd     = "echo 'update: consul/agent.crt' && sighup-consul"
      content = <<EOT
{{ with secret "${local.consul_secret}" "common_name=${local.consul_name}" "ttl=730h"}}
{{ .Data.certificate }}
{{ end }}
EOT
    },
    {
      name    = "consul.agent.key.tpl"
      dest    = "${var.private_keys_base_folder}/consul.key"
      cmd     = "echo 'update: consul.key' && sighup-consul"
      content = <<EOT
{{ with secret "${local.consul_secret}" "common_name=${local.consul_name}" "ttl=730h"}}
{{ .Data.private_key }}
{{ end }}
EOT
    },
  ]
  # end templates

  certificate_stanzas = [
    for template in local.certificate_templates : <<EOT
template {
  source          = "/opt/consul-template/templates/${template.name}"
  destination     = "${template.dest}"
  command         = "${template.cmd}"
  command_timeout = "60s"
  perms           = "0644"
}
EOT
  ]

  certificate_config = templatefile(
    "${path.module}/templates/consul-template.config.hcl",
    {
      vault_address = var.vault_address
      vault_token   = var.vault_token_for_consul_template
      stanzas       = join("\n", local.certificate_stanzas)
    }
  )

  certificate_files = [
    for template in local.certificate_templates : {
      name    = "${template.name}"
      content = base64encode("${template.content}")
    }
  ]

  certificate_consul_base64 = <<EOT
configure-consul-template \
  --name "certificate-templates" \
  --templates "${base64encode(jsonencode(local.certificate_files))}" \
  --configuration "${base64encode(local.certificate_config)}"
EOT

}

output "certificate_consul_base64" {
  value = local.certificate_consul_base64
}
