variable "region" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "external_domain" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "unreal_port" {
  type        = number
  default     = 4712
  description = "Websocket port for Unreal engines"
}
variable "player_port" {
  type        = number
  default     = 4713
  description = "Websocket port for players"
}

variable "twilio_id" {
  type        = string
  default     = "twilio_id"
}

variable "twilio_token" {
  type        = string
  default     = "twilio_token"
}

job "pixelstreaming" {
  region      = var.region
  datacenters = [var.datacenter]
  type        = "service"

  group "emcee" {
    count = 1

    # constraint {
    #   attribute = attr.platform.gce.tag.ingress
    #   value     = "true"
    # }

    network {
      mode = "bridge"
      port "ws_unreal" {}
      port "ws_player" {}
    }

    service {
      name = "emcee-player"
      port = "ws_player"
      task = "emcee"
      tags = [
        "traefik.enable=true",
        # "traefik.consulcatalog.connect=true",
        # ! Attention
        # Routing via TCP (not HTTP) using HostSNI
        "traefik.tcp.routers.emcee-player.entrypoints=player",
        "traefik.tcp.routers.emcee-player.rule=HostSNI(`*`)",
        "traefik.tcp.routers.emcee-player.tls=true",
        "traefik.tcp.routers.emcee-player.service=emcee-player",
        "traefik.tcp.services.emcee-player.loadbalancer.server.port=${NOMAD_PORT_ws_player}",
      ]
      connect {
        sidecar_service {}
      }
    }

    service {
      name = "emcee-unreal"
      port = "ws_unreal"
      task = "emcee"
      tags = [
        "traefik.enable=true",
        # ! Attention
        # Routing via TCP (not HTTP) using HostSNI
        "traefik.tcp.routers.emcee-unreal.entrypoints=unreal",
        "traefik.tcp.routers.emcee-unreal.rule=HostSNI(`*`)",
        # ! Attention: no TLS for engine socket
        "traefik.tcp.routers.emcee-unreal.tls=false",
        "traefik.tcp.routers.emcee-unreal.service=emcee-unreal",
        "traefik.tcp.services.emcee-unreal.loadbalancer.server.port=${NOMAD_PORT_ws_unreal}",
      ]
      connect {
        sidecar_service {}
      }
    }

    task "emcee" {
      driver = "docker"
      config {
        image      = "lowi/pxl-emcee"
        privileged = true
        ports = [
          "ws_unreal",
          "ws_player",
        ]
        force_pull = true
      }

      env {
        UNREAL_PORT = NOMAD_PORT_ws_unreal
        PLAYER_PORT = NOMAD_PORT_ws_player
      }


      template {
        data        = <<EOF
TWILIO_ACCOUNT_SID="${var.twilio_id}"
TWILIO_AUTH_TOKEN="${var.twilio_token}"
EOF
        destination = "secrets/twilio"
        perms       = "600"
        change_mode = "noop"
        env         = true
      }
    }
  }

  group "web" {
    count = 1
    network {
      mode = "bridge"
      port "http" {
        to = 80
      }
    }

    service {
      name = "pxl-web"
      port = "http"
      task = "web"
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.pxl-web.entrypoints=websecure",
        "traefik.http.routers.pxl-web.rule=Host(`pxl.${var.external_domain}`)",
        "traefik.http.routers.pxl-web.tls",
        "traefik.http.routers.pxl-web.service=pxl-web",
      ]

      # // check {
      # //   type     = "http"
      # //   path     = "/"
      # //   interval = "2s"
      # //   timeout  = "2s"
      # // }

      connect {
        sidecar_service {}
      }
    }

    task "web" {
      driver = "docker"
      config {
        image = "lowi/pxl-web:latest"
        ports = ["http"]
        force_pull = true
      }
      env {
        VITE_PUBLIC_SIGNALING_URL = "wss://${var.external_domain}:8443"
      }

    }
  }
}
