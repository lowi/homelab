terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}


resource "nomad_job" "emcee" {
  jobspec = file("${path.module}/pixelstreaming.nomad")
  hcl2 {
    enabled = true
    vars = {
      datacenter      = var.datacenter
      region          = var.region
      external_domain = var.external_domain
      unreal_port     = var.pixelstreaming_unreal_port
      player_port     = var.pixelstreaming_player_port
      twilio_id       = var.twilio_id
      twilio_token    = var.twilio_token
    }
  }
}
