variable "datacenter" {
  default     = "jumpstr"
  description = "Data Center that the job needs to be run in"
}

variable "external_domain" {
  default     = "example.com"
  description = "The public domain"
}

// variable "home_assistant_environment" {
//   description = "The Bitwarden environemnt variables"
// }

// variable "volume_id" {
//   description = "The ID of the volume we're mounting"
// }


job "home_assistant" {
  datacenters =  ["${var.datacenter}"]
  type        = "service"

  group "home_assistant" {
    count = 1

    network {
      mode = "bridge"
      port "http" {
        static = 8123
        to = 8123
      }
    }

    // volume "home_assistant" {
    //   type            = "csi"
    //   read_only       = false
    //   source          = "${var.volume_id}"
    //   access_mode     = "multi-node-multi-writer"
    //   attachment_mode = "file-system"
    // }

    service {
      name = "home-assistant"
      task = "home_assistant"
      port = "http"
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.home_assistant.entrypoints=web",
        "traefik.http.routers.home_assistant.rule=Host(`home-assistant.${var.external_domain}`)",
        "traefik.http.services.home_assistant.loadbalancer.server.port=${NOMAD_PORT_http}",
      ]


      // check {
      //   name     = "Bitwarden HTTP"
      //   type     = "http"
      //   path     = "/-/healthy"
      //   interval = "10s"
      //   timeout  = "2s"
      // }

      connect {
        sidecar_service {}
      }
    }

    restart {
      attempts = 3
      interval = "5m"
      delay    = "30s"
      mode     = "fail"
    }

    // ephemeral_disk {
    //   size = 300
    // }

    task "home_assistant" {

      driver = "docker"

      // volume_mount {
      //   volume      = "${var.volume_id}"
      //   destination = "/data"
      //   read_only   = false
      // }

      config {
        image = "ghcr.io/home-assistant/home-assistant:stable"
        privileged = true
        volumes = [
          "local/config:/config",
        ]
        // network_mode = "host"
        ports = ["http"]
      }

      env {
        TZ="Europe/Vienna"
      }

      // template {
      //   data        = var.home_assistant_environment
      //   destination = "local/config/"
      //   env         = true
      // }

    //   resources {
    //       cpu    = 500
    //       memory = 128
    //   }
    }
  }
}
