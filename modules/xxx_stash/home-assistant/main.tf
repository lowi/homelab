terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

// resource "nomad_external_volume" "bitwarden" {
//   type      = "csi"
//   plugin_id = "kadalu-csi"
//   volume_id = "bitwarden"
//   name      = "bitwarden"

//   capacity_min = "200M"
//   capacity_max = "2G"

//   capability {
//     access_mode     = "multi-node-multi-writer"
//     attachment_mode = "file-system"
//   }

//   parameters = {
//     kadalu_format   = "native"
//     storage_name    = var.volume_name
//     gluster_hosts   = var.gluster_hosts
//     gluster_volname = var.gluster_volume_name
//   }
// }

// data "template_file" "bitwarden_environment" {
//   template = file("${path.module}/conf/bitwarden.env")
//   vars = {
//   }
// }

resource "nomad_job" "home_assistant" {
  // depends_on = [nomad_external_volume.bitwarden, data.template_file.bitwarden_environment]

  jobspec = file("${path.module}/home_assistant.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"            = var.datacenter,
      "external_domain"       = var.external_domain,
    }
  }
}
