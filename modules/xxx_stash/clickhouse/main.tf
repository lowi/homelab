terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_external_volume" "clickhouse" {
  type      = "csi"
  plugin_id = var.csi_plugin_id
  volume_id = "clickhouse"
  name      = "clickhouse"

  capacity_min = "200M"
  capacity_max = "200G"

  capability {
    access_mode     = "single-node-writer"
    attachment_mode = "file-system"
  }

  parameters = {
    kadalu_format   = "native"
    storage_name    = var.gluster_volume_name
    gluster_hosts   = var.gluster_hosts
    gluster_volname = var.gluster_brick_name
  }
}

data "template_file" "clickhouse_config" {
  template = file("${path.module}/clickhouse-config.xml")
  vars = {}
}

data "template_file" "clickhouse_user_config" {
  template = file("${path.module}/clickhouse-user-config.xml")
  vars = {}
}


resource "nomad_job" "postgres" {
  jobspec = file("${path.module}/clickhouse.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"             = var.datacenter,
      "location"               = var.location,
      "external_domain"        = var.external_domain,
      "volume_id"              = nomad_external_volume.clickhouse.volume_id
      "clickhouse_config"      = data.template_file.clickhouse_config.rendered,
      "clickhouse_user_config" = data.template_file.clickhouse_user_config.rendered,
    }
  }
}
