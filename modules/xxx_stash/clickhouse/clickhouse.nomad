variable "location" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "external_domain" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "volume_id" {
  description = "Id of the csi volume"
}

variable "clickhouse_config" {
  description = "Clickhouse configuration"
}

variable "clickhouse_user_config" {
  description = "Clickhouse user configuration"
}

job "clickhouse" {
  region      = var.location
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "clickhouse" {

    network {
      mode = "bridge"
      port "clickhouse_db" {
        static = 5432
        to     = 5432
      }
      port "ui" {
        static = 5050
        to     = 5050
      }
    }

    volume "clickhouse" {
      type            = "csi"
      read_only       = false
      source          = "${var.volume_id}"
      access_mode     = "single-node-writer"
      attachment_mode = "file-system"
    }


    service {
      name = "clickhouse"
      task = "clickhouse"
      check {
        name     = "alive"
        type     = "tcp"
        path     = "/"
        interval = "10s"
        timeout  = "2s"
      }
    }

    restart {
      attempts = 2
      interval = "2m"
      delay    = "25s"
      mode     = "fail"
    }

    task "clickhouse" {
      driver = "docker"

      volume_mount {
        volume      = var.volume_id
        destination = "/var/lib/clickhouse"
        read_only   = false
      }

      template {
        data        = var.clickhouse_config
        destination = "/etc/clickhouse-server/config.d/logging.xml"
      }

      template {
        data        = var.clickhouse_user_config
        destination = "/etc/clickhouse-server/users.d/logging.xml"
      }

      config {
        image = "yandex/clickhouse-server:21.3.2.5"
        ulimit {
          memlock = "-1"
          nofile  = "65536"
          nproc   = "4096"
        }
      }
    }
  }
}

