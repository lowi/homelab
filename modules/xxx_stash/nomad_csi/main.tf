// https://gist.github.com/angrycub/54ccec8215b4c5f405f95b98f1faa3b5

locals {
  credentials_key = "credentials/csi"
}

# Create a service acount capapble of attaching disks
module "service-accounts" {
  source     = "terraform-google-modules/service-accounts/google"
  version    = "4.1.1"
  project_id = var.project_id
  names      = ["compute-storage"]
  project_roles = [
    "${var.project_id}=>roles/compute.storageAdmin",
    "${var.project_id}=>roles/compute.instanceAdmin",
    "${var.project_id}=>roles/iam.serviceAccountUser",
  ]
  display_name  = "CSI service account"
  description   = "Service account for managing CSI disks"
  generate_keys = true
}

# Put the credentials into Consul
# TODO: Put credentials into **Vault**
resource "consul_keys" "csi_credentials" {
  depends_on = [module.service-accounts]
  key {
    path  = local.credentials_key
    value = module.service-accounts.key
  }
}

resource "nomad_job" "csi_node" {
  depends_on       = [consul_keys.csi_credentials]
  jobspec          = file("${path.module}/node.nomad")
  detach           = false
  purge_on_destroy = true
  hcl2 {
    enabled = true
    vars = {
      "csi_plugin_id"    = var.csi_plugin_id,
      "csi_driver_image" = var.csi_driver_image,
      "datacenter"       = var.datacenter,
      "credentials_key"  = local.credentials_key,
    }
  }
}

resource "nomad_job" "csi_controller" {
  depends_on = [nomad_job.csi_node]

  jobspec          = file("${path.module}/controller.nomad")
  detach           = false
  purge_on_destroy = true
  hcl2 {
    enabled = true
    vars = {
      "csi_plugin_id"    = var.csi_plugin_id,
      "csi_driver_image" = var.csi_driver_image,
      "datacenter"       = var.datacenter,
      "credentials_key"  = local.credentials_key,
    }
  }
}
