variable "datacenter" {
  type        = string
  description = "Nomad datacenter"
}

variable "csi_plugin_id" {
  type        = string
  description = "Name of CSI plugin"
}

variable "csi_driver_image" {
  type        = string
  description = "The container image of the CSI driver"
}

variable "credentials_key" {
  type        = string
  description = "KV key for retrieving the GCO credentials.json"
}

# see: https://github.com/carlosrbcunha/hashitalks2021

job "plugin-csi-controller" {
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "controller" {
    task "plugin" {
      driver = "docker"

      template {
        destination = "secrets/gce_credentials.json"
        data = <<EOH
{{ key "${var.credentials_key}" }}
EOH
      }

      env {
        GOOGLE_APPLICATION_CREDENTIALS = "/secrets/gce_credentials.json"
      }

      config {
        image = "${var.csi_driver_image}"

        // volumes = [
        //   "local/gce_credentials.json:/etc/nomad.d/gce_credentials.json"
        // ]

        args = [
          "-endpoint=unix:///csi/csi.sock",
          "-v=6",
          "-logtostderr",
          "-run-node-service=false",
        ]
      }

      csi_plugin {
        id        = "${var.csi_plugin_id}"
        type      = "controller"
        mount_dir = "/csi"
      }

      resources {
        cpu    = 64
        memory = 64
      }

      # ensuring the plugin has time to shut down gracefully
      kill_timeout = "2m"
    }
  }
}
