variable "datacenter" {
  type        = string
  description = "Nomad datacenter"
}

variable "csi_plugin_id" {
  type        = string
  description = "Name of csi plugin"
}

variable "csi_driver_image" {
  type        = string
  description = "The container image of the CSI driver"
}

variable "credentials_key" {
  type        = string
  description = "KV key for retrieving the GCO credentials.json"
}


# see: https://github.com/carlosrbcunha/hashitalks2021

job "plugin-csi-node" {
  datacenters = ["${var.datacenter}"]
  type        = "system"

  group "node" {
    task "plugin" {
      driver = "docker"

      template {
        destination = "secrets/gce_credentials.json"
        data = <<EOH
{{ key "${var.credentials_key}" }}
EOH
      }

      env {
        GOOGLE_APPLICATION_CREDENTIALS = "/secrets/gce_credentials.json"
      }

      config {
        image = "${var.csi_driver_image}"

        args = [
          "--run-controller-service=false",
          "-endpoint=unix:///csi/csi.sock",
          "-v=6",
          "-logtostderr",
        ]

        # node plugins must run as privileged jobs because they
        # mount disks to the host
        privileged = true
      }

      csi_plugin {
        id        = "${var.csi_plugin_id}"
        type      = "node"
        mount_dir = "/csi"
      }

      resources {
        cpu    = 64
        memory = 64
      }

      # ensuring the plugin has time to shut down gracefully
      kill_timeout = "2m"
    }
  }
}
