terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "p1x3l_emcee" {
  jobspec = file("${path.module}/emcee.p1x3l.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"      = var.datacenter
      "location"        = var.location
      "external_domain" = var.external_domain
      "engine_port"     = var.pixelstreaming_engine_port
      "player_port"     = var.pixelstreaming_player_port
      "twilio_id"       = var.twilio_id
      "twilio_token"    = var.twilio_token
      "tags"            = var.nomad_job_tags
    }
  }
}
