
variable "twilio_id" {
  type = string
  description = "Twilio login id"
}

variable "twilio_token" {
  type = string
  description = "Twilio login token"
}
