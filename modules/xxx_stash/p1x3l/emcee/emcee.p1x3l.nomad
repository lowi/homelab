variable "location" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "external_domain" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "docker_image" {
  description = "The docker image we're using"
  default     = "lowi/emcee:latest"
}

variable "engine_port" {
  type        = number
  default     = 4712
  description = "Websocket port for engines"
}
variable "player_port" {
  type        = number
  default     = 4713
  description = "Websocket port for players"
}

variable "twilio_id" {
  type        = string
  description = "Twilio login id"
}

variable "twilio_token" {
  type        = string
  description = "Twilio login token"
}

variable "tags" {
  default     = ""
  type        = string
  description = "Comma separated list of tags that will be attached to the nomad job"
}

job "emcee" {
  region      = var.location
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "emcee" {
    count = 1

    network {
      mode = "bridge"
      port "ws_engine" {
        static = var.engine_port
        to     = var.engine_port
      }
      port "ws_player" {
        static = var.player_port
        to     = var.player_port
      }
    }

    service {
      name = "emcee"
      port = "ws_player"
      task = "emcee"

      tags = concat(
        split(",", var.tags),
        [
          "traefik.enable=true",
          // ! Attention
          // Routing via TCP (not HTTP) using HostSNI
          "traefik.tcp.routers.emcee.entrypoints=wss",
          "traefik.tcp.routers.emcee.rule=HostSNI(`*`)",
          "traefik.tcp.routers.emcee.tls=true",
          "traefik.tcp.routers.emcee.tls.certresolver=le",
          "traefik.tcp.routers.emcee.tls.domains[0].main=${var.external_domain}",
          "traefik.tcp.routers.emcee.tls.domains[0].sans=*.${var.external_domain}",
          "traefik.tcp.routers.emcee.service=emcee",
          "traefik.tcp.services.emcee.loadbalancer.server.port=${NOMAD_PORT_ws_player}",
      ])

      connect {
        sidecar_service {}
      }
    }

    task "emcee" {
      driver = "docker"
      config {
        image = "lowi/emcee"
        ports = [
          "ws_engine",
          "ws_player",
        ]
        // force_pull = true
      }

      env {
        UNREAL_PORT        = "${NOMAD_PORT_ws_engine}"
        PLAYER_PORT        = "${NOMAD_PORT_ws_player}"
        TWILIO_ACCOUNT_SID = "${var.twilio_id}"
        TWILIO_AUTH_TOKEN  = "${var.twilio_token}"
      }

      // resources {
      //   cpu    = 128
      //   memory = 128
      // }
    }
  }
}
