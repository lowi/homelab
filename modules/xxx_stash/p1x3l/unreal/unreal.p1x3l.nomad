variable "location" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "external_domain" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "script" {
  description = "The script to execute"
}

variable "res_x" {
  default     = "1280"
  description = "X Resolution"
}

variable "res_y" {
  default     = "720"
  description = "X Resolution"
}

variable "emcee_ip" {
  description = "The IP address of the pixelstreaming emcee"
  default = "localhost"
}

variable "port" {
  description = "The TCP port of the pixelstreaming emcee"
  default = "8888"
}

variable "user" {
  description = "The user used to start the process"
  default = "nomad"
}

job "unreal" {
  region      = var.location
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "unreal" {
    count = 1

    constraint {
      attribute = "${meta.gpu}"
      operator  = "="
      value     = "true"
    }

    // network {
    //   mode = "bridge"
    //   port "ws_unreal" {}
    //   port "ws_player" {}
    // }

    // service {
    //   name = "unreal"
    //   port = "ws_player"
    //   task = "unreal"

    //   connect {
    //     sidecar_service {}
    //   }
    // }

    task "unreal" {
      driver = "raw_exec"
      // user = var.user
      config {
        command = var.script
        args = [
          "-AudioMixer",
          "-PixelStreamingIP=${var.emcee_ip}",
          "-PixelStreamingPort=${var.port}",
          "-WinX=0",
          "-WinY=0",
          "-ResX=${var.res_x}",
          "-ResY=${var.res_y}",
          // "-Windowed",
          "-RenderOffScreen",
          "-ForceRes",
          "-AllowPixelStreamingCommands",
          "-PixelStreamingHideCursor",
          "-FullStdOutLogOutput",
        ]
    }

    // resources {
    //   cpu    = 128
    //   memory = 128
    // }
  }
}
}
