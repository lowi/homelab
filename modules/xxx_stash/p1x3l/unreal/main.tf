terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "p1x3l_unreal" {
  jobspec = file("${path.module}/unreal.p1x3l.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"      = var.datacenter
      "location"        = var.location
      "external_domain" = var.external_domain
      "script"          = var.pixelstreaming_script_path
      "res_x"           = var.pixelstreaming_res_x
      "res_y"           = var.pixelstreaming_res_y
      "emcee_ip"        = var.pixelstreaming_emcee_ip
      "port"            = var.pixelstreaming_engine_port
      "user"            = var.pixelstreaming_user
    }
  }
}



// data "nomad_job" "p1x3l_unreal" {
//   depends_on = [resource.nomad_job.p1x3l_unreal]
//   job_id = resource.nomad_job.p1x3l_unreal.id
// }

// output "debug" {
//   value = var.script_path
// }
