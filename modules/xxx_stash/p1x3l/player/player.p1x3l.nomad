variable "location" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "external_domain" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "tags" {
  default     = ""
  type = string
  description = "Comma separated list of tags that will be attached to the nomad job"
}

job "player" {
  region      = var.location
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "player" {
    count = 1

    network {
      mode = "bridge"
      port "http" {
        to = 3000
        static = 3000
      }
    }

    service {
      name = "player"
      port = "http"
      task = "player"

      tags = concat(
        split(",", var.tags),
        [
          "traefik.enable=true",
          "traefik.http.routers.p1x3l_player.entrypoints=websecure",
          "traefik.http.routers.p1x3l_player.rule=Host(`play.${var.external_domain}`)",
          "traefik.http.routers.p1x3l_player.tls",
          "traefik.http.routers.p1x3l_player.tls.certresolver=le",
          "traefik.http.routers.p1x3l_player.tls.domains[0].main=${var.external_domain}",
          "traefik.http.routers.p1x3l_player.tls.domains[0].sans=*.${var.external_domain}",
          "traefik.http.routers.p1x3l_player.service=p1x3l_player",
          "traefik.http.services.p1x3l_player.loadbalancer.server.port=${NOMAD_PORT_http}",
        ])

      connect {
        sidecar_service {}
      }
    }

    task "player" {
      driver = "docker"
      config {
        image = "lowi/player:latest"
        ports = [
          "http",
        ]
        force_pull = true
      }

      // resources {
      //   cpu    = 128
      //   memory = 128
      // }
    }
  }
}
