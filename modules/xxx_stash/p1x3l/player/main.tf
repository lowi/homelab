terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "p1x3l_player" {
  jobspec = file("${path.module}/player.p1x3l.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"      = var.datacenter
      "location"        = var.location
      "external_domain" = var.external_domain
      "tags"            = var.nomad_job_tags
    }
  }
}
