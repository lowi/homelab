variable "location" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "external_domain" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "traefik_config" {
  description = "The Traefik configuration toml"
}

variable "http_port" {
  description = "The port bound to HTTP traffic"
  default     = "80"
}

variable "https_port" {
  description = "The port bound to HTTPS traffic"
  default     = "443"
}

variable "wss_port" {
  description = "The port bound to SecureWebSocket traffic"
  default     = "8443"
}

variable "traefik_port" {
  description = "The port bound to the dashboard endpoint"
  default     = "8080"
}

// variable "namecheap_user" {
// }

// variable "namecheap_key" {
// }

// variable "acme_json" {
//   description = "The contents of our letsencrypt json file"
//   type = string
// }


job "traefik" {
  region      = var.location
  datacenters = ["${var.datacenter}"]
  type        = "service"

  // deploy in the public subnet
  // constraint {
  //   attribute = "${meta.vpc}"
  //   operator  = "="
  //   value     = "public"
  // }

  group "traefik" {
    count = 1

    network {
      mode = "bridge"
      port "http" {
        static = var.http_port
        to     = var.http_port
      }
      port "https" {
        static = var.https_port
        to     = var.https_port
      }
      port "wss" {
        static = var.wss_port
        to     = var.wss_port
      }
      port "traefik" {
        static = var.traefik_port
        to     = var.traefik_port
      }
    }

    service {
      name = "traefik"
      port = "traefik"
      task = "traefik"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.traefik.entrypoints=websecure",
        "traefik.http.routers.traefik.rule=Host(`traefik.${var.external_domain}`)",
        "traefik.http.routers.traefik.tls",
        "traefik.http.routers.traefik.tls.certresolver=le",
        "traefik.http.routers.traefik.tls.domains[0].main=${var.external_domain}",
        "traefik.http.routers.traefik.tls.domains[0].sans=*.${var.external_domain}",
        "traefik.http.routers.traefik.service=traefik",
        "traefik.http.services.traefik.loadbalancer.server.port=${NOMAD_PORT_traefik}",
      ]

      // check {
      //   name     = "alive"
      //   type     = "http"
      //   port     = 8080
      //   path     = "/ping"
      //   interval = "10s"
      //   timeout  = "2s"
      // }

      connect {
        native = true
      }
    }

    task "traefik" {
      driver = "docker"
      config {
        image = "traefik:2.6"
        ports = [
          "http",
          "https",
          "wss",
          "traefik",
        ]

        /*
          "If you set the group network.mode = "bridge" you should not
            set the Docker config network_mode, or the container will be unable
            to reach other containers in the task group. This will also prevent
            Connect-enabled tasks from reaching the Envoy sidecar proxy."
          https://www.nomadproject.io/docs/drivers/docker#network_mode
        */
        // ! do NOT set the network_mode

        volumes = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
          "local/acme.json:/acme.json",
        ]
      }

      env {
        NAMECHEAP_API_USER = "${var.namecheap_user}"
        NAMECHEAP_API_KEY  = "${var.namecheap_key}"
      }

      template {
        data        = var.traefik_config
        destination = "local/traefik.toml"
      }

      template {
        data        = var.acme_json
        destination = "local/acme.json"
      }

      resources {
        cpu    = 128
        memory = 128
      }
    }
  }
}
