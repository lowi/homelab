
variable "http_port" {
  description = "The port bound to HTTP traffic"
  type        = number
  default     = 80
}

variable "https_port" {
  description = "The port bound to HTTPS traffic"
  type        = number
  default     = 443
}

variable "wss_port" {
  description = "The port bound to SecureWebSocket traffic"
  type        = number
  default     = 8443
}

variable "traefik_port" {
  description = "The port bound to the dashboard endpoint"
  type        = number
  default     = 8080
}

// variable "api_port" {
//   description = "The port bound to the traefik API"
//   type = number
//   default = 8081
// }

// variable "metrics_port" {
//   description = "The port bound to the traefik metrics endpoint"
//   type = number
//   default = 8083
// }

// variable "p1x3l_player_port" {
//   description = "The TCP port for player WebSocket connections."
//   type = number
//   default = 8181
// }
