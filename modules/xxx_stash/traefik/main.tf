terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

data "template_file" "traefik_config" {
  template = file("${path.module}/traefik.toml")
  vars = {
    datacenter     = var.datacenter
    location       = var.location

    // ! Attention
    // The consul agent is liteneing on 0.0.0.0 (i.e. all available interfaces)
    // '172.17.0.1' is the IP of the Docker interface
    consul_url     = "172.17.0.1:8500"

    http_port      = var.http_port
    https_port     = var.https_port
    traefik_port   = var.traefik_port
    wss_port       = var.wss_port
    ssl_cert_email = var.ssl_cert_email
    acme_provider  = var.acme_provider
  }
}

resource "nomad_job" "traefik" {
  jobspec = file("${path.module}/traefik.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"      = var.datacenter,
      "location"        = var.location,
      "external_domain" = var.external_domain
      "traefik_config"  = data.template_file.traefik_config.rendered
      "http_port"       = var.http_port
      "https_port"      = var.https_port
      "traefik_port"    = var.traefik_port
      "namecheap_user"  = var.namecheap_user
      "namecheap_key"   = var.namecheap_key
      "acme_json"       = var.acme_json
    }
  }
}
