variable "datacenter" {
  type        = string
  description = "Nomad datacenter"
}

variable "external_domain" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "location" {
  description = "Azure location"
}

variable "tags" {
  default     = ""
  type = string
  description = "Comma separated list of tags that will be attached to the nomad job"
}


job "portainer" {
  region      = var.location
  datacenters = ["${var.datacenter}"]
  type        = "service"

  // deploy in the public subnet
  // constraint {
  //   attribute = "${meta.vpc}"
  //   operator  = "="
  //   value     = "public"
  // }

  group "portainer" {
    count = 1

    network {
      mode = "bridge"
      port "http" {
        static = 9000
        to     = 9000
      }
      port "https" {
        static = 9443
        to     = 9443
      }
    }

    service {
      name = "portainer"
      port = "http"
      task = "portainer"

      tags = concat(

        split(",", var.tags),

        [
          "traefik.enable=true",
          "traefik.http.routers.portainer.entrypoints=websecure",
          "traefik.http.routers.portainer.rule=Host(`portainer.${var.external_domain}`)",
          "traefik.http.routers.portainer.tls",
          "traefik.http.routers.portainer.tls.certresolver=le",
          "traefik.http.routers.portainer.tls.domains[0].main=${var.external_domain}",
          "traefik.http.routers.portainer.tls.domains[0].sans=*.${var.external_domain}",
          "traefik.http.routers.portainer.service=portainer",
          "traefik.http.services.portainer.loadbalancer.server.port=${NOMAD_PORT_http}",
        ]
      )

      // check {
      //   name     = "alive"
      //   type     = "http"
      //   port     = "http"
      //   path     = "/"
      //   interval = "10s"
      //   timeout  = "2s"
      // }

      connect {
        sidecar_service {}
      }
    }

    task "portainer" {
      driver = "docker"
      config {
        image = "portainer/portainer-ce:latest"
        volumes = [
          "${NOMAD_TASK_DIR}:/data",
          "/var/run/docker.sock:/var/run/docker.sock",
        ]
        ports = [ "http", "https" ]
        privileged = true
      }
      resources {
        cpu    = 128
        memory = 128
      }
    }
  }
}
