variable "datacenter" {
  type        = string
  description = "Nomad datacenter"
}

variable "external_domain" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "location" {
  description = "Azure location"
}

variable "tags" {
  default     = ""
  type = string
  description = "Comma separated list of tags that will be attached to the nomad job"
}


job "networking_tools" {
  region      = var.location
  datacenters = ["${var.datacenter}"]
  type        = "service"

  // deploy in the public subnet
  constraint {
    attribute = "${meta.vpc}"
    operator  = "="
    value     = "public"
  }

  group "networking_tools" {
    count = 1

    network {
      mode = "bridge"
    }

    task "networking_tools" {
      driver = "docker"
      config {
        image = "lowi/networking_tools:latest"
        privileged = true
        force_pull = true
      }
    }
  }
}
