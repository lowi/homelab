terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "portainer" {
  jobspec = file("${path.module}/portainer.nomad")
  hcl2 {
    enabled = true
    vars = {
      "external_domain" = var.external_domain,
      "location"        = var.region,
      "datacenter"      = var.datacenter,
      "tags"            = var.nomad_job_tags,
    }
  }
}

// resource "nomad_job" "networking_tools" {
//   jobspec = file("${path.module}/networking_tools.nomad")
//   hcl2 {
//     enabled = true
//     vars = {
//       "external_domain" = var.external_domain,
//       "location"        = var.location,
//       "datacenter"      = var.datacenter,
//       "tags"            = var.nomad_job_tags,
//     }
//   }
// }
