terraform {
  required_version = ">= 0.16"
  required_providers {
    nomad = {
      source  = "hashicorp/nomad"
      version = "1.4.16"
    }
  }
}

resource "nomad_job" "mamoto" {
  jobspec = file("${path.module}/mamoto.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"      = var.datacenter
      "location"        = var.location
      "external_domain" = var.external_domain
      "docker_image"    = var.docker_image
      "twilio_id"       = var.twilio_id
      "twilio_token"    = var.twilio_token
    }
  }
}
