variable "location" {
  default = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job is running in"
}

variable "external_domain" {
  default     = "example.com"
  description = "The publicly available base URL"
}


job "mamoto" {
  region      = var.location
  datacenters = ["${var.datacenter}"]
  type        = "service"

  group "mamoto" {
    count = 1

    network {
      mode = "bridge"
      port "http" {
        to = 3000
      }
    }

    service {
      name = "mamoto"
      port = "http"
      task = "mamoto"

      tags = [
        "traefik.enable=true",
        "traefik.http.routers.mamoto.entrypoints=web",
        "traefik.http.routers.mamoto.rule=Host(`analytics.${var.public_base_url}`)",
        "traefik.http.services.mamoto.loadbalancer.server.port=${NOMAD_PORT_http}",
      ]

      connect {
        sidecar_service {}
      }
    }

    task "mamoto" {
      driver = "docker"
      config {
        image = "mamoto"
        ports = [
          "http",
        ]
        // force_pull = true
      }

      env {
        UNREAL_PORT        = "${NOMAD_PORT_ws_engine}"
        PLAYER_PORT        = "${NOMAD_PORT_ws_player}"
        TWILIO_ACCOUNT_SID = "${var.twilio_id}"
        TWILIO_AUTH_TOKEN  = "${var.twilio_token}"
      }

      // resources {
      //   cpu    = 128
      //   memory = 128
      // }
    }
  }
}
