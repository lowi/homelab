
resource "nomad_external_volume" "loki" {
  type      = "csi"
  plugin_id = "kadalu-csi"
  volume_id = "loki"
  name      = "loki"

  capacity_min = "200M"
  capacity_max = "1G"

  capability {
    access_mode     = "multi-node-multi-writer"
    attachment_mode = "file-system"
  }

  parameters = {
    kadalu_format   = "native"
    storage_name    = var.volume_name
    gluster_hosts   = var.gluster_hosts
    gluster_volname = var.gluster_volume_name
  }
}


data "template_file" "loki_config" {
  template = file("${path.module}/conf/loki.yml")
}

resource "nomad_job" "loki" {
  jobspec = file("${path.module}/conf/loki.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"      = var.datacenter,
      "volume_id"       = nomad_external_volume.loki.volume_id,
      "public_base_url" = var.public_base_url,
      "loki_config"     = data.template_file.loki_config.rendered
    }
  }
}

