

resource "nomad_external_volume" "prometheus" {
  type      = "csi"
  plugin_id = "kadalu-csi"
  volume_id = "prometheus"
  name      = "prometheus"

  capacity_min = "200M"
  capacity_max = "1G"

  capability {
    access_mode     = "multi-node-multi-writer"
    attachment_mode = "file-system"
  }

  parameters = {
    kadalu_format   = "native"
    storage_name    = var.volume_name
    gluster_hosts   = var.gluster_hosts
    gluster_volname = var.gluster_volume_name
  }
}

data "template_file" "prometheus_config" {
  template = file("${path.module}/conf/prometheus.yml")
  vars = {
    consul_url = var.consul_url
  }
}

resource "nomad_job" "prometheus" {
  depends_on = [nomad_external_volume.prometheus, data.template_file.prometheus_config]

  jobspec = file("${path.module}/conf/prometheus.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"        = var.datacenter,
      "volume_id"         = nomad_external_volume.prometheus.volume_id
      "public_base_url"   = var.public_base_url,
      "prometheus_config" = data.template_file.prometheus_config.rendered
    }
  }
}

// resource "nomad_job" "grafana" {
//   jobspec = file("${path.module}/conf/grafana.nomad")
//   hcl2 {
//     enabled = true
//     vars = {
//       "datacenter" = var.datacenter,
//     }
//   }
// }
