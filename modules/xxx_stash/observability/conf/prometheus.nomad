variable "datacenter" {
  default     = "jumpstr"
  description = "Data Center that the job needs to be run in"
}

variable "public_base_url" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "prometheus_config" {
  description = "The Prometheus configuration yml"
}

variable "volume_id" {
  description = "The ID of the volume we're mounting"
}


job "prometheus" {
  datacenters =  ["${var.datacenter}"]
  type        = "service"

  group "monitoring" {
    count = 1

    network {
      mode = "bridge"
      port "http" {
        to     = 9090
        static = 9090
      }
    }

    volume "prometheus" {
      type            = "csi"
      read_only       = false
      source          = "${var.volume_id}"
      access_mode     = "multi-node-multi-writer"
      attachment_mode = "file-system"
    }

    service {
      name = "prometheus"
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.prometheus.entrypoints=web",
        "traefik.http.routers.prometheus.rule=Host(`prometheus.${var.public_base_url}`)",
        "traefik.http.services.prometheus.loadbalancer.server.port=9090",
      ]

      port = "http"

      check {
        name     = "Prometheus HTTP"
        type     = "http"
        path     = "/-/healthy"
        interval = "10s"
        timeout  = "2s"
      }

      connect {
        sidecar_service {}
      }
    }

    restart {
      attempts = 3
      interval = "5m"
      delay    = "30s"
      mode     = "fail"
    }

    // ephemeral_disk {
    //   size = 300
    // }

    task "prometheus" {

      driver = "docker"

      volume_mount {
        volume      = "${var.volume_id}"
        destination = "/prometheus"
        read_only   = false
      }

      config {
        image = "prom/prometheus:latest"
        args = [
          "--config.file=/etc/prometheus/prometheus.yml",
          "--storage.tsdb.path=/prometheus",
          "--web.console.libraries=/etc/prometheus/console_libraries",
          "--web.console.templates=/etc/prometheus/consoles",
          "--storage.tsdb.retention.time=30d",
          "--web.enable-lifecycle"
        ]
        volumes = [
          "local/prometheus.yml:/etc/prometheus/prometheus.yml",
        ]
        ports = ["http"]
      }

      template {
        data = var.prometheus_config
        destination = "local/prometheus.yml" # Rendered template.
        change_mode = "restart"
      }

      resources {
        memory = 400
      }
    }
  }
}
