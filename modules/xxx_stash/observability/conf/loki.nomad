
variable "public_base_url" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "datacenter" {
  default     = "jumpstr"
  description = "Data Center that the job needs to be run in"
}

variable "volume_id" {
  default     = ""
  description = "The ID of the volume we're mounting"
}

variable "loki_config" {
  default     = ""
  description = "The Loki configuration yml"
}

job "loki" {
  datacenters =  ["${var.datacenter}"]
  type        = "service"

  group "logging" {
    count = 1

    network {
      mode = "bridge"
      port "http" {
        to     = 3100
        static = 3100
      }
    }

    volume "loki" {
      type            = "csi"
      read_only       = false
      source          = "${var.volume_id}"
      access_mode     = "multi-node-multi-writer"
      attachment_mode = "file-system"
    }

    service {
      name = "loki"
      port = "http"
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.loki.entrypoints=web",
        "traefik.http.routers.loki.rule=Host(`loki.${var.public_base_url}`)",
        "traefik.http.services.loki.loadbalancer.server.port=9090",
      ]

      check {
        name     = "Loki HTTP"
        type     = "http"
        path     = "/ready"
        interval = "5s"
        timeout  = "2s"

        check_restart {
          limit           = 2
          grace           = "60s"
          ignore_warnings = false
        }
      }

      connect {
        sidecar_service {}
      }
    }

    restart {
      attempts = 3
      interval = "5m"
      delay    = "30s"
      mode     = "fail"
    }

    ephemeral_disk {
      size = 300
    }

    task "loki" {

      driver = "docker"

      volume_mount {
        volume      = "${var.volume_id}"
        destination = "/data"
        read_only   = false
      }

      config {
        image = "grafana/loki:latest"
        privileged = true

        args = [
          "-config.file",
          "/etc/loki/loki.yml",
        ]
        volumes = [
          "local/loki.yml:/etc/loki/loki.yml",
        ]
        ports = ["http"]
      }

      template {
        data = var.loki_config
        destination = "local/loki.yml"
        change_mode = "restart"
      }

      resources {
        cpu    = 50
        memory = 32
      }
    }
  }
}
