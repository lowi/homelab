variable "datacenter" {
  default     = "jumpstr"
  description = "Data Center that the job needs to be run in"
}

variable "grafana_config" {
  description = "The Grafana configuration yml"
}

job "grafana" {
  datacenters =  ["${var.datacenter}"]
  type        = "service"

  group "dashboard" {
    count = 1

    network {
      mode = "bridge"
      port "http" {
        to     = 3000
        static = 3000
      }
    }

    service {
      name = "grafana"
      tags = ["grafana", "web"]
      port = "http"

      connect {
        sidecar_service {}
      }

      check {
        name     = "Grafana HTTP"
        type     = "http"
        path     = "/api/health"
        interval = "5s"
        timeout  = "2s"

        check_restart {
          limit           = 2
          grace           = "60s"
          ignore_warnings = false
        }
      }
    }

    // ephemeral_disk {
    //   size = 300
    // }

    restart {
      attempts = 3
      interval = "5m"
      delay    = "30s"
      mode     = "fail"
    }

    task "grafana" {
      driver = "docker"
      # https://www.nomadproject.io/docs/job-specification/task#user
      # https://grafana.com/docs/grafana/latest/installation/docker/#migrate-to-v51-or-later
      user = "root"

      config {
        image = "grafana/grafana:8.0.0-ubuntu"
        ports = ["http"]
      }

      env {
        GF_LOG_LEVEL          = "DEBUG"
        GF_LOG_MODE           = "console"
        GF_SERVER_HTTP_PORT   = "${NOMAD_PORT_http}"
        GF_PATHS_PROVISIONING = "/local/grafana/provisioning"
      }

      template {
        data = var.grafana_config
        destination = "/local/grafana/provisioning/datasources/ds.yaml"
        change_mode = "restart"
      }

      resources {
        cpu    = 200
        memory = 200
      }
    }
  }
}

