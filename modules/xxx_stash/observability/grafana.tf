
data "template_file" "grafana_config" {

  template = file("${path.module}/conf/grafana.yml")
  vars = {
    prometheus_url = local.prometheus_url
    loki_url       = local.loki_url
  }
}

resource "nomad_job" "grafana" {
  depends_on = [
    nomad_job.prometheus,
    nomad_job.loki,
    data.template_file.grafana_config
  ]

  jobspec = file("${path.module}/conf/grafana.nomad")
  hcl2 {
    enabled = true
    vars = {
      "datacenter"     = var.datacenter,
      "grafana_config" = data.template_file.grafana_config.rendered
    }
  }
}

locals {
  prometheus_url = "http://prometheus.${var.service_domain}:9090"
  loki_url       = "http://loki.${var.service_domain}:3100"
}
