
// variable "nomad_url" {
//   type        = string
//   description = "The URL of a Nomad node"
//   default     = "http://127.0.0.1:4646"
// }

variable "nomad_region" {
  type        = string
  description = "The Nomad region."
  default     = "global"
}

variable "datacenter" {
  default     = "dc1"
  description = "Data Center that the job needs to be run in"
}

variable "public_base_url" {
  default     = "example.com"
  description = "The publicly available base URL"
}

variable "consul_url" {
  type        = string
  description = "The URL of the consul cluster (without protocol)"
  default     = "127.0.0.1:8500"
}

variable "service_domain" {
  type        = string
  description = "The domain of the services registered with consul"
  // default     = "service.consul"
}

variable "consul_token" {
  type        = string
  description = "Access token for Consul"
  default     = "xxx"
}

variable "gluster_hosts" {
  default     = "gluster-server"
  description = <<-EOS
    - External gluster host where the gluster volume is created, started and quota is set
    - Multiple hosts can be supplied like "host1,host2,host3" (no spaces and trimmed endings)
    - Prefer to supply only one or else need to supply the same wherever interpolation is not supported (ex: in volume.hcl files)
    EOS
}

variable "volume_name" {
  default     = "kadalu-pool"
  description = "Volume name for Kadalu CSI which is used for all PVC creations purposes"
}

variable "gluster_volume_name" {
  default     = "vol1"
  description = "Gluster volume name in external cluster"
}
