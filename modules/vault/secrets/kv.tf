
# Enable PKI secrets engine at vault_consul_ica_path.
resource "vault_mount" "secret_kv" {
  path                      = var.vault_secrets_path
  type                      = "kv-v2"
  description               = "Secret KV store"
}

resource "time_sleep" "wait_for_kv" {
  depends_on = [vault_mount.secret_kv]
  create_duration = "10s"
}
