
# Root certificate authority
# ————————————————————————————————————————————————————————————————


# Enable PKI secrets engine
resource "vault_consul_secret_backend" "consul" {
  path        = "consul"
  type        = "consul"
  description = "Consul access control"
  address     = "127.0.0.1:8500"
}

# resource "vault_consul_secret_backend" "test" {
#   path        = "consul"
#   description = "Manages the Consul backend"
#   address     = "127.0.0.1:8500"
#   token       = "4240861b-ce3d-8530-115a-521ff070dd29"
# }
# resource "vault_pki_secret_backend_config_urls" "root" {
#   backend                 = vault_mount.root.path
#   issuing_certificates    = ["${var.vault_address}/v1/${vault_mount.root.path}/ca"]
#   crl_distribution_points = ["${var.vault_address}/v1/${vault_mount.root.path}/crl"]
# }

# Generate a signing request for the root
resource "vault_pki_secret_backend_intermediate_cert_request" "root" {
  backend      = vault_mount.root.path
  type         = "internal"
  common_name  = "Internal Root CA"
  key_type     = "rsa"
  key_bits     = "2048"
  ou           = var.ca_subject.organizational_unit
  organization = var.ca_subject.organization
  country      = var.ca_subject.country
}

# Now run `sign_certificate_request.sh` (in terraform/scripts)
# The run `root_ca_02`
