# https://www.nomadproject.io/docs/integrations/vault-integration

# * The policy given to the nomad servers.
# * it is used by the servers to to create tokens 
# * for the roles given to the noamd-clients
# ? everything inside `nomadserver.policy.hcl` is about issuing tokens to clients
# ? These client-tokens are used by nomad-jobs to access vault-secrets
data "template_file" "nomad_server_policy" {
  template = file("${path.module}/templates/nomadserver.policy.hcl")
  vars = {
    role = var.nomad_role
  }
}
resource "vault_policy" "nomad_server_policy" {
  name   = "nomad-server"
  policy = data.template_file.nomad_server_policy.rendered
}

# * The policy given to the nomad tasks.
# * defined in the nomad-server-configuration as the policy
# * that is given to noman-jobs
# ? in our case this only allows reading from the /secret kv-store 
# ? see the role defined below
data "template_file" "access_kv_policy" {
  template = file("${path.module}/templates/access-kv.policy.hcl")
  vars = {
    kv   = var.vault_secrets_path
  }
}
resource "vault_policy" "access_kv_policy" {
  name   = "access-kv"
  policy = data.template_file.access_kv_policy.rendered
}


# * Here we define the role that is given to the nomad-clients
resource "vault_token_auth_backend_role" "nomad_role" {
  role_name              = var.nomad_role
  allowed_policies       = ["access-kv"]
  token_explicit_max_ttl = 0
  orphan                 = true
  token_period           = 259200
  renewable              = true
}
