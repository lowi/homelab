
# Allow looking up the token passed to Nomad to validate # the token has the
# proper capabilities. This is provided by the "default" policy.
path "auth/token/lookup-self" {
  capabilities = ["read"]
}

# Allow looking up incoming tokens to validate they have permissions to access
# the tokens they are requesting. This is only required if
# `allow_unauthenticated` is set to false.
path "auth/token/lookup" {
  capabilities = ["update"]
}

# Allow checking the capabilities of our own token. This is used to validate the
# token upon startup.
path "sys/capabilities-self" {
  capabilities = ["update"]
}

# Allow our own token to be renewed.
path "auth/token/renew-self" {
  capabilities = ["update"]
}

path "/sys/mounts" {
  capabilities = [ "read" ]
}

path "/sys/mounts/${root_pki}" {
  capabilities = [ "read" ]
}

path "/sys/mounts/${intermediate_pki}" {
  capabilities = [ "read" ]
}

path "/${root_pki}/" {
  capabilities = [ "read" ]
}

path "/${root_pki}/root/sign-intermediate" {
  capabilities = [ "update" ]
}

path "/${intermediate_pki}/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}
