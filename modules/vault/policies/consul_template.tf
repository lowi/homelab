# https://learn.hashicorp.com/tutorials/consul/vault-pki-consul-connect-ca?in=vault/secrets-management


locals {

  consul_template_policy = templatefile(
    "${path.module}/templates/consul-template.policy.hcl",
    {
      kv = var.vault_secrets_path
      intermediate_pki = "${var.intermediate_pki_path}/"
    }
  )
}

resource "vault_policy" "consul_template_policy" {
  name   = "consul-template"
  policy = local.consul_template_policy
}

