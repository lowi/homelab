# https://learn.hashicorp.com/tutorials/consul/vault-pki-consul-connect-ca?in=vault/secrets-management

data "template_file" "connect_policy" {
  template = file("${path.module}/templates/connect.policy.hcl")
  vars = {
    role             = var.connect_role
    root_pki         = var.root_pki_path
    intermediate_pki = var.intermediate_pki_path
  }
}

resource "vault_policy" "connect_policy" {
  name   = "connect"
  policy = data.template_file.connect_policy.rendered
}

