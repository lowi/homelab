# https://learn.hashicorp.com/tutorials/consul/vault-pki-consul-connect-ca?in=vault/secrets-management

data "template_file" "consul_policy" {
  template = file("${path.module}/templates/consul.policy.hcl")
  vars = {
    role             = var.consul_role
    root_pki         = var.root_pki_path
    intermediate_pki = var.intermediate_pki_path
  }
}

resource "vault_policy" "consul_policy" {
  name   = "consul"
  policy = data.template_file.consul_policy.rendered
}

