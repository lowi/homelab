
# Intermediate CA
# ————————————————————————————————————————————————————————————————

# How to: Generate mTLS Certificates for Consul with Vault
# https://learn.hashicorp.com/tutorials/consul/vault-pki-consul-secure-tls


# Generate mTLS Certificates for Nomad using Vault
# https://learn.hashicorp.com/tutorials/nomad/vault-pki-nomad


# Create mount path
resource "vault_mount" "intermediate_ca" {
  path                      = var.intermediate_pki_path
  type                      = "pki"
  description               = "${var.namespace} Intermediate CA"
  max_lease_ttl_seconds     = local.one_year_in_seconds
  default_lease_ttl_seconds = local.one_year_in_seconds
}

# Request a certificate
resource "vault_pki_secret_backend_intermediate_cert_request" "intermediate_ca" {
  backend      = vault_mount.intermediate_ca.path
  type         = "internal"
  common_name  = "${var.namespace} Intermediate CA"
  format       = "pem"
  key_type     = "rsa"
  key_bits     = "2048"
  ou           = var.ca_subject.organizational_unit
  organization = var.ca_subject.organization
  country      = var.ca_subject.country
}

# Sign the certificate request with the Vault CA
resource "vault_pki_secret_backend_root_sign_intermediate" "intermediate_ca" {
  backend              = var.root_pki_path
  csr                  = vault_pki_secret_backend_intermediate_cert_request.intermediate_ca.csr
  common_name          = "${var.namespace} Intermediate CA"
  exclude_cn_from_sans = true
  ou                   = var.ca_subject.organizational_unit
  organization         = var.ca_subject.organization
  ttl                  = local.one_year_in_seconds
}

# Set the signed certificate
resource "vault_pki_secret_backend_intermediate_set_signed" "intermediate_ca" {
  backend = vault_mount.intermediate_ca.path
  certificate = join("\n", [
    vault_pki_secret_backend_root_sign_intermediate.intermediate_ca.certificate,
    file(var.vault_ica_certificate),
  ])
}


# Verification
# ———————————————————————————————————————————————————————————————

resource "null_resource" "intermediate_ca" {
  depends_on = [vault_pki_secret_backend_intermediate_set_signed.intermediate_ca]
  triggers = {
    timestamp = timestamp()
  }

  # Expected result:
  # subject=O = jumpstr, OU = Department of Certificate Authority, CN = jumpstr Intermediate CA
  # issuer=C = AT, O = jumpstr, OU = Department of Certificate Authority, CN = Internal Root CA
  provisioner "local-exec" {
    command = "curl -s ${var.vault_address}/v1/${var.intermediate_pki_path}/ca/pem | openssl crl2pkcs7 -nocrl -certfile  /dev/stdin  | openssl pkcs7 -print_certs -noout"
  }

  # Expected result:
  # subject=O = jumpstr, OU = Department of Certificate Authority, CN = jumpstr Intermediate CA
  # issuer=C = AT, O = jumpstr, OU = Department of Certificate Authority, CN = Internal Root CA
  # subject=C = AT, O = jumpstr, OU = Department of Certificate Authority, CN = Internal Root CA
  # issuer=C = AT, ST = Vienna, O = jumpstr, OU = Department of Certificate Authority, CN = jumpstr Root CA
  # subject=C = AT, ST = Vienna, O = jumpstr, OU = Department of Certificate Authority, CN = jumpstr Root CA
  # issuer=C = AT, ST = Vienna, O = jumpstr, OU = Department of Certificate Authority, CN = jumpstr Root CA
  provisioner "local-exec" {
    command = "curl -s ${var.vault_address}/v1/${var.intermediate_pki_path}/ca_chain | openssl crl2pkcs7 -nocrl -certfile  /dev/stdin  | openssl pkcs7 -print_certs -noout"
  }
}



# Roles
# ————————————————————————————————————————————————————————————————

locals {
  # We have one intermediate CA for all Hashistack servers,
  # so we need to make sure thate we allow all required domains
  intermediate_ca_roles = {
    # "vault" = {
    #   name   = var.intermediate_ica_vault_role,
    #   domain = "vault"
    # },
    "consul" = {
      name   = var.consul_role,
      domain = "${var.datacenter}.${var.consul_domain}"
    },
    "nomad" = {
      name   = var.nomad_role,
      domain = "${var.datacenter}.${var.nomad_domain}"
    },
    "connect" = {
      name   = var.connect_role,
      domain = "${var.service_domain}"
    },
  }
}

resource "vault_pki_secret_backend_role" "intermediate_ca" {
  depends_on = [
    vault_pki_secret_backend_intermediate_set_signed.intermediate_ca
  ]
  for_each = local.intermediate_ca_roles

  backend            = var.intermediate_pki_path
  name               = each.value.name
  max_ttl            = local.one_month_in_seconds
  ttl                = local.one_hour_in_seconds
  allow_any_name     = false
  allow_localhost    = true
  allowed_domains    = [each.value.domain]
  allow_subdomains   = true
  allow_bare_domains = false
  generate_lease     = true
  allow_ip_sans      = true
  key_usage          = ["DigitalSignature"]
  key_type           = "rsa"
  key_bits           = 2048
  server_flag        = false
  client_flag        = true
  no_store           = true
}

