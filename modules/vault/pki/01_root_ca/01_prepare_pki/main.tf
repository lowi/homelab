
# Root certificate authority
# ————————————————————————————————————————————————————————————————


# Enable PKI secrets engine
resource "vault_mount" "root" {
  path                      = var.root_pki_path
  type                      = "pki"
  description               = "Internal Root CA"
  default_lease_ttl_seconds = local.one_year_in_seconds
  max_lease_ttl_seconds     = local.three_years_in_seconds
}


# resource "vault_pki_secret_backend_config_urls" "root" {
#   backend                 = vault_mount.root.path
#   issuing_certificates    = ["${var.vault_address}/v1/${vault_mount.root.path}/ca"]
#   crl_distribution_points = ["${var.vault_address}/v1/${vault_mount.root.path}/crl"]
# }

# Generate a signing request for the root
resource "vault_pki_secret_backend_intermediate_cert_request" "root" {
  backend      = vault_mount.root.path
  type         = "internal"
  common_name  = "Internal Root CA"
  key_type     = "rsa"
  key_bits     = "2048"
  ou           = var.ca_subject.organizational_unit
  organization = var.ca_subject.organization
  country      = var.ca_subject.country
}

# Now run `sign_certificate_request.sh` (in terraform/scripts)
# The run `root_ca_02`
