
# Root certificate authority
# ————————————————————————————————————————————————————————————————
# continued

# Run this module ofther the Vault certificate request has been signed

# Finnish the Vault ICA configuration
# by setting the certificate signed by our offline CA
# see: https://learn.hashicorp.com/tutorials/vault/pki-engine-external-ca#step-3-generate-ica1-in-vault
resource "vault_pki_secret_backend_intermediate_set_signed" "root" {
  backend     = var.root_pki_path
  certificate = file(var.vault_ica_certificate)
}

# curl -s $VAULT_ADDR/v1/vault-ca/ca/pem | openssl crl2pkcs7 -nocrl -certfile  /dev/stdin  | openssl pkcs7 -print_certs -noout
# curl -s $VAULT_ADDR/v1/vault-ca/ca_chain | openssl crl2pkcs7 -nocrl -certfile  /dev/stdin  | openssl pkcs7 -print_certs -noout


# Verification
# ———————————————————————————————————————————————————————————————

resource "null_resource" "root_ca" {
  depends_on = [vault_pki_secret_backend_intermediate_set_signed.root]
  triggers = {
    timestamp = timestamp()
  }

  # The result should be:
  #   subject=O = p1x3l, OU = Department of Certificate Authority, CN = p1x3l Vault CA
  #   issuer=O  = p1x3l, OU = Department of Certificate Authority, CN = p1x3l Root CA
  provisioner "local-exec" {
    command = "curl -s ${var.vault_address}/v1/${var.root_pki_path}/ca/pem | openssl crl2pkcs7 -nocrl -certfile  /dev/stdin  | openssl pkcs7 -print_certs -noout"
  }

  provisioner "local-exec" {
    command = "curl -s ${var.vault_address}/v1/${var.root_pki_path}/ca_chain | openssl crl2pkcs7 -nocrl -certfile  /dev/stdin  | openssl pkcs7 -print_certs -noout"
  }
}
