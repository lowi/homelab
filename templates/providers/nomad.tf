# Use the Default Subscription defined in the Azure CLI

provider "nomad" {
  address      = "${address}"
  region       = "${region}"
}
