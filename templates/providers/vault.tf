
provider "vault" {
  address      = "${address}"
  token        = "${token}"
  token_name   = "${token_name}"
  ca_cert_file = "${ca_cert_file}"
  ca_cert_dir  = "${ca_cert_dir}"
  auth_login   = "${auth_login}"
}
