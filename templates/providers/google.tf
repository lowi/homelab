
provider "google" {
  credentials = file("${credentials_file_path}")
  project     = "${project_id}"
  region      = "${region}"
  zone        = "${zone}"
}
