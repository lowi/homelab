provider "postgresql" {
  host            = "${host}"
  port            = "${port}"
  database        = "${database}"
  username        = "${username}"
  password        = "${password}"
  sslmode         = "disable"
  connect_timeout = 15
}
