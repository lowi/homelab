

terraform {
  required_providers {
    grafana = {
      source = "grafana/grafana"
      version = "1.20.1"
    }
  }
}

provider "grafana" {
  url  = var.url
  auth = var.auth
}
