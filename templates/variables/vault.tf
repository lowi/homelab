//  ██████╗  ██████╗███████╗
// ██╔════╝ ██╔════╝██╔════╝
// ██║  ███╗██║     ███████╗
// ██║   ██║██║     ╚════██║
// ╚██████╔╝╚██████╗███████║
//  ╚═════╝  ╚═════╝╚══════╝
variable "storage_bucket_name" {
  type        = string
  default     = ""
  description = <<EOF
Name of the Google Cloud Storage bucket for the Vault backend storage. This must
be globally unique across of of GCP. If left as the empty string, this will
default to: "<project-id>-vault-data".
EOF
}

variable "storage_bucket_location" {
  type        = string
  default     = "us"
  description = <<EOF
Location for the Google Cloud Storage bucket in which Vault data will be stored.
EOF
}

variable "storage_bucket_class" {
  type        = string
  default     = "MULTI_REGIONAL"
  description = <<EOF
Type of data storage to use. If you change this value, you will also need to
choose a storage_bucket_location which matches this parameter type.
EOF
}

variable "storage_bucket_enable_versioning" {
  type        = string
  default     = false
  description = <<EOF
Set to true to enable object versioning in the GCS bucket.. You may want to
define lifecycle rules if you want a finite number of old versions.
EOF
}

variable "storage_bucket_lifecycle_rules" {
  type = list(object({
    action    = map(any)
    condition = map(any)
  }))
  default     = []
  description = <<EOF
If you enable versioning, you may want to expire old versions to configure
a specific retention. Please, check the documentation for the map keys you
should use.
This is specified as a list of objects:
    storage_lifecycle_rules = [
      {
        action = {
          type = "Delete"
        }
        conditions = {
          age     = 60
          is_live = false
        }
      }
    ]
EOF
}

variable "storage_bucket_force_destroy" {
  type        = string
  default     = false
  description = <<EOF
Set to true to force deletion of backend bucket on `terraform destroy`.
EOF
}


// ██╗ █████╗ ███╗   ███╗
// ██║██╔══██╗████╗ ████║
// ██║███████║██╔████╔██║
// ██║██╔══██║██║╚██╔╝██║
// ██║██║  ██║██║ ╚═╝ ██║
// ╚═╝╚═╝  ╚═╝╚═╝     ╚═╝
variable "vault_service_account_name" {
  type        = string
  default     = "vault-admin"
  description = <<EOF
Name of the Vault service account.
EOF
}

variable "vault_service_account_iam_roles" {
  type = list(string)
  default = [
    "roles/logging.logWriter",
    "roles/monitoring.metricWriter",
    "roles/monitoring.viewer",
  ]
  description = <<EOF
List of IAM roles for the Vault admin service account to function.
EOF
}

variable "vault_service_account_storage_bucket_iam_roles" {
  type = list(string)
  default = [
    "roles/storage.legacyBucketReader",
    "roles/storage.objectAdmin",
  ]
  description = <<EOF
List of IAM roles for the Vault admin service account to have on the storage
bucket.
EOF
}


// ██╗  ██╗███╗   ███╗███████╗
// ██║ ██╔╝████╗ ████║██╔════╝
// █████╔╝ ██╔████╔██║███████╗
// ██╔═██╗ ██║╚██╔╝██║╚════██║
// ██║  ██╗██║ ╚═╝ ██║███████║
// ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝

variable "kms_keyring" {
  type        = string
  default     = "vault"
  description = <<EOF
Name of the Cloud KMS KeyRing for asset encryption. Terraform will create this
keyring.
EOF
}

variable "kms_crypto_key" {
  type        = string
  default     = "vault-init"
  description = <<EOF
The name of the Cloud KMS Key used for encrypting initial TLS certificates and
for configuring Vault auto-unseal. Terraform will create this key.
EOF
}

variable "kms_protection_level" {
  type        = string
  default     = "software"
  description = <<EOF
The protection level to use for the KMS crypto key.
EOF
}
