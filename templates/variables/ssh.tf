variable "ssh_username" {
  type        = string
  description = "The ssh username"
}

variable "ssh_public_key_file" {
  type        = string
  description = "The public key for the ssh user"
}

variable "ssh_private_key_file" {
  type        = string
  description = "The path to the private key for the ssh user"
}

variable "ssh_common_args" {
  type        = string
  description = "Common ssh argumets for ansible"
}
