variable "namespace" {
  type        = string
  description = "Our namespace"
}

variable "external_domain" {
  type        = string
  description = "Public domain name"
}

variable "service_domain" {
  type        = string
  description = "Internal domain for consul services"
}

variable "datacenter" {
  type        = string
  description = "Name of our datacenter"
  default     = "dc1"
}

variable "consul_tag" {
  type        = string
  description = "Internal Consul namespace"
  default     = "default-consul-tag"
}

variable "nomad_tag" {
  type        = string
  description = "Internal Nomad namespace"
  default     = "default-nomad-tag"
}

variable "unreal_tag" {
  type        = string
  description = "Internal Unreal namespace"
  default     = "default-unreal-tag"
}
