

variable "csi_version" {
    description = "Version number of the csi plugin"
    default = "0.8.10"
}

variable "csi_plugin_id" {
    description = "Name of csi plugin"
}

// variable "managed_disks" {
//   type = list(object({
//     volume_name = string
//     disk_size   = number
//     type        = string
//   }))
// }

// variable "disk_create_option" {
//   type        = string
//   default     = "Empty"
//   description = "Disk creation option"
// }

// variable "os_disk_size" {
//   type        = number
//   description = "Disk size of the disk (in GB)"
// }

// variable "csi_prefix" {
//   type        = string
//   description = "Prefix for all csi resources"
// }

