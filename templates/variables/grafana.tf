variable "grafana_url" {
  type        = string
  description = "Grafana URL"
}

variable "grafana_auth" {
  type        = string
  description = "Grafana authentication token"
}
