
# Nomad client variabels
# ——————————————————————

variable "nomad_client_image" {
  type        = string
  description = "The VM image for nomad clients"
  default     = ""
}

variable "nomad_client_machine_type" {
  description = "The machine type of the Compute Instance to run for each node in the cluster."
  type        = string
  default     = "g1-micro"
}

variable "nomad_client_volume_disk_type" {
  description = "The GCE disk type. Can be either pd-ssd, local-ssd, or pd-standard"
  type        = string
  default     = "pd-standard"
}

variable "nomad_client_network" {
  description = "The name of the VPC where all resources should be created."
  type        = string
  default     = null
}

variable "nomad_client_subnet" {
  description = "The name of the VPC Subnetwork where all resources should be created. "
  type        = string
  default     = null
}

variable "nomad_client_tags" {
  description = "A list of tags to assign to the Compute Instance"
  type        = list(string)
  default     = []
}

variable "nomad_client_metadata" {
  description = "A map of metadata key value pairs to assign to the Compute Instance metadata."
  type        = map(string)
  default     = {}
}
