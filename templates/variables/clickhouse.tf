
variable "clickhouse_admin_email" {
  type        = string
  description = "Admin email address"
}

variable "clickhouse_admin_name" {
  type        = string
  description = "Admin username"
}

variable "clickhouse_admin_pwd" {
  type        = string
  description = "Admin password"
}

variable "clickhouse_secret_key_base" {
  type        = string
  description = "Secret base ky"
}
