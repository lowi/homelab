
variable "location" {
  type        = string
  description = "The Azure location"
}

variable "resource_group_name" {
  type        = string
  description = "Name of the Azure resource group"
}

variable "datacenter" {
  type        = string
  description = "The name of our datacenter"
}

variable "public_ip_id" {
  type        = string
  description = "The Azure Id of our public IP address"
}

variable "public_ip" {
  type        = string
  description = "Public IPv4 address"
}

variable "subscription_id" {
  type = string
}
variable "tenant_id" {
  type = string
}
variable "client_id" {
  type = string
}
variable "client_secret" {
  type = string
  sensitive = true
}
