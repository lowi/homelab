
variable "nomad_job_tags" {
  type        = string
  description = "Comma separated list of tags that will be attached to the nomad job"
  default     = "foo,bar"
}
