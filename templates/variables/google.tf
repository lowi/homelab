
variable "credentials_file_path" {
  type        = string
  description = "Local path to the GCP credentials.json file"
}

variable "project_id" {
  type        = string
  description = "Id of the GCP project"
}

variable "region" {
  type        = string
  description = "GCP region"
}

variable "zone" {
  type        = string
  description = "GCP zone"
}

variable "public_ip" {
  type        = string
  description = "The publicly accessible IP address of the Bastion"
}

variable "service_account" {
  type        = string
  description = "Email address of the Terraform service account"
  default     = "<name>@<PROJECT_ID>.iam.gserviceaccount.com"
}

variable "admin_account" {
  type        = string
  description = "admin@example.com"
}

variable "google_services" {
  type = list(string)
  default = [ "compute.googleapis.com" ]
  description = <<EOF
List of services to enable on the project. These services
are required in order for this Vault setup to function.
To disable, set to the empty list []. You may want to disable this if the
services have already been enabled and the current user does not have permission
to enable new services.
EOF
}
