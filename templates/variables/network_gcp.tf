variable "external_address_name" {
  type        = string
  description = "The name of our external address"
}

variable "datacenter" {
  type        = string
  description = "The name of our datacenter (Consul, Nomad)"
}

variable "location" {
  type        = string
  description = "The location in which our infrastructure resides"
}

variable "internal_domain" {
  type        = string
  description = "https://cloud.google.com/compute/docs/internal-dns#zonal-dns"
}

variable "network_name" {
  type        = string
  description = "The name of our (VPC) network"
}

variable "public_subnet_name" {
  type        = string
  description = "The name of the public subnet"
}

variable "private_subnet_name" {
  type        = string
  description = "The name of the private subnet"
}

variable "public_subnet_cidr" {
  type        = string
  description = "CIDR block of the public subnet"
  default     = "10.26.1.0/24"
}

variable "private_subnet_cidr" {
  type        = string
  description = "CIDR block of the private subnet"
  default     = "10.26.2.0/24"
}

variable "cloud_router_name" {
  type        = string
  description = "Name of the cloud router"
  default     = "my-cloud-router"
}

variable "nat_gateway_name" {
  type        = string
  description = "Name of the NAT gateway"
  default     = "my-nat-gateway"
}

variable "internal_dns" {
  type        = string
  description = "The internal DNS resolver"
  default     = "8.8.8.8"
}
