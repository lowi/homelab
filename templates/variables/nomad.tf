
variable "nomad_cluster_size" {
  type        = number
  description = "The size of our Nomad cluster"
  default     = 3
}

variable "nomad_prefix" {
  type        = string
  description = "Prefix for all resources"
}

variable "nomad_tag" {
  type        = string
  description = "The name of our cluster"
}

variable "nomad_cluster_url" {
  type        = string
  description = "The URL where the consul cluster can be reached"
}

variable "nomad_image" {
  type        = string
  description = "The vm image for nomad servers"
  default = ""
}

variable "nomad_hostnames" {
  type        = list(string)
  description = "The hostnames of our Nomad cluster nodes"
}

variable "nomad_cluster_subnet_id" {
  type        = string
  description = "The id of the subnet in which the Nomad cluster is running"
  default     = "public"
}

variable "nomad_vm_size" {
  type        = string
  description = "The machine instance size"
  default = ""
}

variable "nomad_client_image" {
  type        = string
  description = "The machine image for nomad clients"
  default = ""
}

variable "nomad_client_vm_size" {
  type        = string
  description = "The machine instance szie"
  default = ""
}

variable "nomad_client_meta" {
  type        = map(string)
  description = "Metadata for client nodes"
  default = {}
}

