variable "unreal_machine_type" {
  type = string
  description = ""
}

variable "unreal_image" {
  type = string
  description = "VM image id"
}

variable "unreal_subnet" {
  type = string
  description = "The subnet in which the Unreal machines are running"
}

variable "unreal_metadata_key_name_for_cluster_size" {
  description = "The key name to be used for the custom metadata attribute that represents the size of the Nomad cluster."
  type        = string
  default     = "cluster-size"
}

variable "unreal_max_cluster_size" {
  type = number
  description = "The number of unreal machines in the cluster"
  default = 0
}

variable "unreal_client_meta" {
  type = map(string)
  description = "Nomad metadata map"
  default = {}
}

variable "unreal_disk_type" {
  type = string
  description = "The type of disk we're attaching to the VM. GCP only"
  default = "pd-standard"
}

variable "unreal_gpu_type" {
  type = string
  description = "The type of GPU we're attaching to the VM. GCP only"
  default = "nvidia-tesla-t4"
}

variable "unreal_gpu_count" {
  type = number
  description = "The number of GPUs we're attaching to the VM. GCP only"
  default = 1
}

variable "pixelstreaming_emcee_ip" {
  description = "The IP address of the pixelstreaming emcee"
  default     = "127.0.0.1"
}

variable "pixelstreaming_engine_port" {
  type = number
  description = "Websocket port for engines"
  default = 4712
}

variable "pixelstreaming_player_port" {
  type = number
  description = "Websocket port for players"
  default = 4713
}

variable "pixelstreaming_script_path" {
  type        = string
  description = "Path to startup script"
}

variable "pixelstreaming_res_x" {
  default     = 1280
  type        = number
  description = "X Resolution"
}

variable "pixelstreaming_res_y" {
  default     = 720
  type        = number
  description = "Y Resolution"
}

variable "pixelstreaming_user" {
  type        = string
  description = "The user running the unreal process"
  default     = "nomad"
}
