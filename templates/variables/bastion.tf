// Variables for the GCP bastion host module

variable "bastion_hostname" {
  type        = string
  description = "Bastion hostname"
  default     = "bastion-vm"
}

variable "bastion_machine_type" {
  type        = string
  description = "GCP machine type for the bastion VM"
  default     = "n1-standard-1"
}

variable "bastion_image_project" {
  type = string
  description = "Project where the source image for the Bastion comes from"
}

variable "bastion_image" {
  type = string
  description = "GCP source image for the bastion VM"
  default = ""
}

variable "bastion_members" {
  type        = list(string)
  description = "List of IAM resources to allow access to the bastion host"
  default = []
}

variable "bastion_service_account" {
  type        = string
  description = "If set, the service account and its permissions will not be created."
}
