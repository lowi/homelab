
variable "acme_provider" {
  description = "Provider for Letsencrypt certificates"
  type = string
}

variable "ssl_cert_email" {
  description = "Contact info for letsencrypt"
  type = string
}

variable "namecheap_user" {
  type = string
}

variable "namecheap_key" {
  type = string
}

variable "acme_json" {
  description = "The contents of our letsencrypt json file"
  type = string
}
