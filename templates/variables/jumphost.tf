variable "jumphost_vm_size" {
  type = string
  description = ""
}

variable "jumphost_image_id" {
  type = string
  description = "Azure VM image id"
}

variable "jumphost_name" {
  type = string
  description = "The hostname"
  default = "publik"
}

variable "jumphost_nomad_client_meta" {
  type = map(string)
  description = "Nomad metadata map"
  default = {
    vpc = "public"
  }
}

// variable "jumphost_nomad_consul_tags" {
//   type = list(string)
//   description = "Nomad tags for consul"
//   default = []
// }
