
# Cluster config
# ——————————————
variable "nomad_cluster_size" {
  type        = number
  description = "The size of our Nomad cluster"
  default     = 3
}

variable "nomad_cluster_url" {
  type        = string
  description = "The URL where the nomad cluster can be reached"
}

variable "nomad_cluster_description" {
  description = "A description of the Nomad cluster; it will be added to the Compute Instance Template."
  type        = string
  default     = null
}

variable "nomad_metadata_key_name_for_cluster_size" {
  description = "The key name to be used for the custom metadata attribute that represents the size of the Nomad cluster."
  type        = string
  default     = "cluster-size"
}

variable "nomad_custom_metadata" {
  description = "A map of metadata key value pairs to assign to the Compute Instance metadata."
  type        = map(string)
  default     = {}
}

# Virtual machines
# ————————————————
variable "nomad_image" {
  type        = string
  description = "The VM image for nomad servers"
  default = ""
}

variable "nomad_machine_type" {
  description = "The machine type of the Compute Instance to run for each node in the cluster."
  type        = string
  default     = "g1-micro"
}

# Disk Settings
# —————————————
variable "nomad_volume_disk_type" {
  description = "The GCE disk type. Can be either pd-ssd, local-ssd, or pd-standard"
  type        = string
  default     = "pd-standard"
}

variable "nomad_network" {
  description = "The name of the VPC where all resources should be created."
  type        = string
  default     = null
}

variable "nomad_subnet" {
  description = "The name of the VPC Subnetwork where all resources should be created. "
  type        = string
  default     = null
}

# Firewall Ports
# ——————————————
variable "nomad_http_port" {
  description = "The port used by Nomad to handle incoming HTPT (API) requests."
  type        = number
  default     = 4646
}

variable "nomad_rpc_port" {
  description = "The port used by Nomad to handle incoming RPC requests."
  type        = number
  default     = 4647
}

variable "nomad_serf_port" {
  description = "The port used by Nomad to handle incoming serf requests."
  type        = number
  default     = 4648
}

variable "nomad_allowed_inbound_cidr_blocks_http" {
  description = "A list of CIDR-formatted IP address ranges from which the Compute Instances will allow connections to Nomad on the port specified by var.http_port."
  type        = list(string)
  default     = []
}

variable "nomad_allowed_inbound_tags_http" {
  description = "A list of tags from which the Compute Instances will allow connections to Nomad on the port specified by var.http_port."
  type        = list(string)
  default     = []
}

variable "nomad_allowed_inbound_cidr_blocks_rpc" {
  description = "A list of CIDR-formatted IP address ranges from which the Compute Instances will allow connections to Nomad on the port specified by var.rpc_port."
  type        = list(string)
  default     = []
}

variable "nomad_allowed_inbound_tags_rpc" {
  description = "A list of tags from which the Compute Instances will allow connections to Nomad on the port specified by var.rpc_port."
  type        = list(string)
  default     = []
}

variable "nomad_allowed_inbound_cidr_blocks_serf" {
  description = "A list of CIDR-formatted IP address ranges from which the Compute Instances will allow connections to Nomad on the port specified by var.serf_port."
  type        = list(string)
  default     = []
}

variable "nomad_allowed_inbound_tags_serf" {
  description = "A list of tags from which the Compute Instances will allow connections to Nomad on the port specified by var.serf_port."
  type        = list(string)
  default     = []
}
