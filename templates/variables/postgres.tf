variable "postgres_volume_id" {
  type        = string
  description = "The ID of the data volume"
}

variable "postgres_secret_path" {
  type        = string
  description = "Path to the postgres secret in Vault"
}
