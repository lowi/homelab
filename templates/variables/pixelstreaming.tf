
variable "pixelstreaming_emcee_ip" {
  description = "The IP address of the pixelstreaming emcee"
  default     = "127.0.01"
}

variable "pixelstreaming_engine_port" {
  type        = number
  description = "Websocket port for engines"
  default     = 4712
}

variable "pixelstreaming_player_port" {
  type        = number
  description = "Websocket port for players"
  default     = 4713
}

variable "pixelstreaming_script_path" {
  description = "Path to startup script"
  type        = string
}

variable "pixelstreaming_res_x" {
  description = "X Resolution"
  type        = number
  default     = 1280
}

variable "pixelstreaming_res_y" {
  description = "Y Resolution"
  type        = number
  default     = 720
}

variable "pixelstreaming_user" {
  type        = string
  description = "The user running the unreal process"
  default     = "nomad"
}
