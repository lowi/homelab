variable "gluster_volume_name" {
  default     = "sample-pool"
  description = "Volume name for Kadalu CSI which is used for all PVC creations purposes"
}

variable "gluster_hosts" {
  default     = "gluster-server"
  description = <<-EOS
    - External gluster host where the gluster volume is created, started and quota is set
    - Multiple hosts can be supplied like "host1,host2,host3" (no spaces and trimmed endings)
    - Prefer to supply only one or else need to supply the same wherever interpolation is not supported (ex: in volume.hcl files)
    EOS
}

variable "gluster_brick_name" {
  default     = "vol1"
  description = "Gluster volume name in external cluster"
}

// variable "glusww
