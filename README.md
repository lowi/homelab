
# Terragrunt infrastructure

This is the main repository for provisioning our infrastructure


## Pre-requisites

1. Install [Terraform](https://www.terraform.io/) version `0.13.0` or newer and
   [Terragrunt](https://github.com/gruntwork-io/terragrunt) version `v0.32.0` or newer.

## Folder structure

The code in this repo uses the following folder hierarchy:

```
environments
 └ jumpstr
   <… terragrunt.hcl files>
 └ azure
    └ staging
    <… terragrunt.hcl files>
modules
  <… terraform modules>
templates
  └ providers
  └ variables

```

### `environments`

The `environments` folder contains the terragrunt configurations for our different environments
At the moment we have 2 environments:

- azure/stage
- jumpstr

`azure/stage` is the (staging) environment for Azure
`jumpstr` is my local development environment

### `modules`

The `modules` folder contains the individual terraform modules called by terragrunt

### `templates` 

The `templates` folder contains 2 sub-folders:

- `providers`
- `varaibles`

#### `providers`

The providers folder contains 3rd-party terraform-providers that are being included into our terragrunt config

#### `variables`

The variables folder contains variable definitions for various components.





## How is the code in this repo organized?


Where:

* **Account**: At the top level are each of your AWS accounts, such as `stage-account`, `prod-account`, `mgmt-account`,
  etc. If you have everything deployed in a single AWS account, there will just be a single folder at the root (e.g.
  `main-account`).

* **Region**: Within each account, there will be one or more [AWS
  regions](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html), such as
  `us-east-1`, `eu-west-1`, and `ap-southeast-2`, where you've deployed resources. There may also be a `_global`
  folder that defines resources that are available across all the AWS regions in this account, such as IAM users,
  Route 53 hosted zones, and CloudTrail.

* **Environment**: Within each region, there will be one or more "environments", such as `qa`, `stage`, etc. Typically,
  an environment will correspond to a single [AWS Virtual Private Cloud (VPC)](https://aws.amazon.com/vpc/), which
  isolates that environment from everything else in that AWS account. There may also be a `_global` folder
  that defines resources that are available across all the environments in this AWS region, such as Route 53 A records,
  SNS topics, and ECR repos.

* **Resource**: Within each environment, you deploy all the resources for that environment, such as EC2 Instances, Auto
  Scaling Groups, ECS Clusters, Databases, Load Balancers, and so on. Note that the Terraform code for most of these
  resources lives in the [terragrunt-infrastructure-modules-example repo](https://github.com/gruntwork-io/terragrunt-infrastructure-modules-example).

## Creating and using root (account) level variables

In the situation where you have multiple AWS accounts or regions, you often have to pass common variables down to each
of your modules. Rather than copy/pasting the same variables into each `terragrunt.hcl` file, in every region, and in
every environment, you can inherit them from the `inputs` defined in the root `terragrunt.hcl` file.
