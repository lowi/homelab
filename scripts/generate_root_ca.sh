#!/bin/bash

cd $SECRETS_BASE_PATH
mkdir tmp && cd tmp

certstrap init \
  --organization "jumpstr" \
  --organizational-unit "Department of Certificate Authority" \
  --country "AT" \
  --province "Vienna" \
  --common-name "jumpstr Root CA" \
  --passphrase "$ROOT_CA_PASSPHRASE" \
  --expires "3 years" \
  --exclude-path-length

# mv out ../root_ca
# cd ..
# rm -rf tmp
