#!/bin/bash

node_names=('mini' 'lenovo' 'pi' 'zwei')

for node_name in "${node_names[@]}"; do
    description="${node_name} agent token"
    node_identity="${node_name}:jumpstr"
    consul acl token create -description "$description" -node-identity "$node_identity"
done
